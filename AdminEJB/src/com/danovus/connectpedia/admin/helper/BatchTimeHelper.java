package com.danovus.connectpedia.admin.helper;

import java.util.ArrayList;
import java.util.List;

import com.danovus.connectpedia.gyminfo.model.CustomBatches;
import com.danovus.connectpedia.gyminfo.model.FitnessGroomingCentre;
import com.danovus.cp.modules.helper.common.vo.CustomBatchesVO;

public class BatchTimeHelper {

	public FitnessGroomingCentre mapCustomBatchTiming(List<CustomBatchesVO> btchList,FitnessGroomingCentre centreDtls){
		
		List<CustomBatches> batchList = new ArrayList<>();
		for(CustomBatchesVO eachBatch : btchList){
			CustomBatches custBatch = new CustomBatches();
			custBatch.setCentreBatchTimeRange(eachBatch.getCentreBatchTimeRange());
			custBatch.setFitnessCentre(centreDtls);
			custBatch.setPackageName(eachBatch.getPackageName());
			batchList.add(custBatch);
		}
		centreDtls.setCustomBatches(batchList);
		return centreDtls;
	}

	public List<CustomBatchesVO> mapFetchBatchTiming(List<CustomBatches> resultList) {
		List<CustomBatchesVO> viewBatchList = new ArrayList<CustomBatchesVO>();
		CustomBatchesVO viewBatch;
		for(CustomBatches eachBatch : resultList){
			viewBatch = new CustomBatchesVO();
			viewBatch.setCentreBatchId(eachBatch.getCentreBatchId());
			viewBatch.setCentreBatchTimeRange(eachBatch.getCentreBatchTimeRange());
			viewBatch.setPackageName(eachBatch.getPackageName());
			viewBatchList.add(viewBatch);
		}
		return viewBatchList;
	}
}

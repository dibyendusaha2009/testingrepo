package com.danovus.connectpedia.admin.helper;

import java.util.ArrayList;
import java.util.List;

import com.danovus.connectpedia.common.model.PackagePayment;
import com.danovus.connectpedia.enroll.model.Enrollment;
import com.danovus.cp.framework.util.DateUtil;
import com.danovus.cp.modules.helper.common.vo.EnrollmentPackageInfoVO;
import com.danovus.cp.modules.helper.vo.User;

public class DietChartHelper {
	
	/*public Enrollment executeMapperForDietChartAdd(User user, Enrollment memberForDietChart){
		for (DietChartVO dietVO : user.getDietChartList()){
			DietChart dietChart = new DietChart();
			dietChart.setCalories(dietVO.getCalories());
			dietChart.setCarbs(dietVO.getCarbs());
			dietChart.setChartBy(dietVO.getChartBy());
			dietChart.setCourseTime(dietVO.getCourseTime());
			dietChart.setDayOfDiet(dietVO.getDayOfDiet());
			dietChart.setDietRemarks(dietVO.getDietRemarks());
			dietChart.setEndDate(dietVO.getEndDate());
			dietChart.setFat(dietVO.getFat());
			dietChart.setItemName(dietVO.getItemName());
			dietChart.setMealTime(dietVO.getMealTime());
			dietChart.setMealType(dietVO.getMealType());
			dietChart.setProtein(dietVO.getProtein());
			dietChart.setQty(dietVO.getQty());
			dietChart.setQtyUnit(dietVO.getQtyUnit());
			dietChart.setStartDate(dietVO.getStartDate());
			dietChart.setEnrollmentForDiet(memberForDietChart);
			memberForDietChart.getDietChart().add(dietChart);
		}
		
		return memberForDietChart;
	}*/
	
	public List<User> executeMapperForMemberSearch(List<Enrollment> resultList) {
		List<User> userList = new ArrayList<>();
		
		for (Enrollment eachEnrollment : resultList) {
			User userVO = new User();
			userVO.setId(eachEnrollment.getEnrId());
			userVO.getPerson().setFirstName(eachEnrollment.getBasicDetails().getName());
			userVO.getPerson().setLastName(eachEnrollment.getBasicDetails().getLastName());
			userVO.getContacts().setContactNum(eachEnrollment.getPersonalDetails().getContactNum());
			if(eachEnrollment.getCustomerAddress() != null )
				userVO.getContacts().setAddress(eachEnrollment.getCustomerAddress().getAddress());
			else
				userVO.getContacts().setAddress("NA");
			List<PackagePayment> enrolledUsersPaymentList = eachEnrollment.getPackagePymnt();
			List<EnrollmentPackageInfoVO> paymentPackageList = new ArrayList<>();
			for(PackagePayment eachPkgPymnt :enrolledUsersPaymentList){
				
				EnrollmentPackageInfoVO eachPayment = new EnrollmentPackageInfoVO();
				
				eachPayment.setId(eachPkgPymnt.getAmountId());
				eachPayment.setDateOfEnrollment(eachPkgPymnt.getEnrollmentDate());
				if(eachPkgPymnt.getMembExpiryDate() != null)
					eachPayment.setDateOfExpiry(DateUtil.getStringFromDate(eachPkgPymnt.getMembExpiryDate(),"dd/MM/yyyy"));
				
				eachPayment.setMembershipAmount(eachPkgPymnt.getMemebershipAmount());
				
				eachPayment.setDateOfActivation(eachPkgPymnt.getActivationDate());
				eachPayment.setTaxInPercentage(eachPkgPymnt.getTaxInPercent());
				eachPayment.setTotalAmount(eachPkgPymnt.getPayableAmount());
				eachPayment.setAssociatedTrainer(eachPkgPymnt.getTrainerName());
				eachPayment.setClosedByStaff(eachPkgPymnt.getBusinessBy());			
				
				eachPayment.setAmountPaid(eachPkgPymnt.getAmountPaid());
				eachPayment.setDiscount(eachPkgPymnt.getDiscount());
				eachPayment.setAmountDue(eachPkgPymnt.getAmountDue());
				eachPayment.setAmountDueDt(eachPkgPymnt.getAmountDueDate());
				
				
				eachPayment.setAmountPaidInCash(eachPkgPymnt.getAmountPaidInCash());
				eachPayment.setAmountPaidInCard(eachPkgPymnt.getAmountPaidInCard());
				eachPayment.setAmountPaidInCheque(eachPkgPymnt.getAmountPaidInCheque());
				eachPayment.setCardNum(eachPkgPymnt.getCardNum());
				eachPayment.setChequeNum(eachPkgPymnt.getChequeNum());
				
				
				
				if(eachPkgPymnt.getIsAmountDueRemaning() != null && eachPkgPymnt.getIsAmountDueRemaning().equalsIgnoreCase("Y"))
					eachPayment.setAmountDueRemaining(Boolean.TRUE);
				else
					eachPayment.setAmountDueRemaining(Boolean.FALSE);
				eachPayment.setMembershipMode(eachPkgPymnt.getMembershipMode());
				eachPayment.setDateOfRenewal(eachPkgPymnt.getRenewalDate());
				eachPayment.setRenewed(eachPkgPymnt.getRenewed());
				eachPayment.setPackageName(eachPkgPymnt.getPackageName());
				eachPayment.setAmountDuePaymentDate(eachPkgPymnt.getAmountDuePymntDate());
				eachPayment.setPaidOn(eachPkgPymnt.getPaidOn());
				eachPayment.setRemarks(eachPkgPymnt.getRemarks());
				eachPayment.setPurpose(eachPkgPymnt.getPurpose());
				if(eachPkgPymnt.getUpgraded() != null && eachPkgPymnt.getUpgraded().equalsIgnoreCase("Y"))
					eachPayment.setUpgraded(Boolean.TRUE);
				else
					eachPayment.setUpgraded(Boolean.FALSE);
				paymentPackageList.add(eachPayment);
				userVO.setPackagePaymentInfo(paymentPackageList);		
			}
			//mapDietChartDetails(eachEnrollment, userVO);
			userList.add(userVO);
		}
		
		
		return userList;
	}
	
	/*private void mapDietChartDetails(Enrollment enrolledUserDetails,User userVO){
		List<DietChart> memDietList = enrolledUserDetails.getDietChart();
		List<DietChartVO> userDietChartList =  new ArrayList<>();
		for (DietChart eachDiet : memDietList){
			DietChartVO userDiet = new DietChartVO();
			userDiet.setCalories(eachDiet.getCalories());
			userDiet.setCarbs(eachDiet.getCarbs());
			userDiet.setChartBy(eachDiet.getChartBy());
			userDiet.setCourseTime(eachDiet.getCourseTime());
			userDiet.setDayOfDiet(eachDiet.getDayOfDiet());
			userDiet.setDietRemarks(eachDiet.getDietRemarks());
			userDiet.setEndDate(eachDiet.getEndDate());
			userDiet.setFat(eachDiet.getFat());
			userDiet.setItemName(eachDiet.getItemName());
			userDiet.setMealTime(eachDiet.getMealTime());
			userDiet.setMealType(eachDiet.getMealType());
			userDiet.setProtein(eachDiet.getProtein());
			userDiet.setQty(eachDiet.getQty());
			userDiet.setQtyUnit(eachDiet.getQtyUnit());
			userDiet.setStartDate(eachDiet.getStartDate());
			userDietChartList.add(userDiet);
		}
		userVO.setDietChartList(userDietChartList);
	}*/

}

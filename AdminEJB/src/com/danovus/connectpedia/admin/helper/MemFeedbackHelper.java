package com.danovus.connectpedia.admin.helper;

import java.util.ArrayList;
import java.util.List;

import com.danovus.connectpedia.common.model.MemberFeedback;
import com.danovus.connectpedia.common.model.MemberFeedbackRatings;
import com.danovus.cp.framework.util.DateUtil;
import com.danovus.cp.modules.helper.common.vo.MemberFeedbackVO;

public class MemFeedbackHelper {

	public MemberFeedback executeMapperForMemFeedbkAdd(MemberFeedbackVO memFeedback){
		MemberFeedback feedbacks = new MemberFeedback();
		
		feedbacks.setFeedbackDate(memFeedback.getFeedbackDate());
		feedbacks.setFeedbackMonth(DateUtil.getMonth(memFeedback.getFeedbackDate()));
		feedbacks.setFeedbackYear(DateUtil.getYear(memFeedback.getFeedbackDate()).toString());
		feedbacks.setFeedbackDetails(memFeedback.getFeedbackDetails());
		feedbacks.setMemberEmail(memFeedback.getMemberEmail());
		feedbacks.setMemberPhone(memFeedback.getMemberPhone());
		feedbacks.setMemberName(memFeedback.getMemberName());
		
		String [] parameterName = memFeedback.getParameterNames();
		int i = 0;
		List<MemberFeedbackRatings> memberRatingList = new ArrayList<>();
		for(Integer rating : memFeedback.getRatings()){
			MemberFeedbackRatings memberRatings = new MemberFeedbackRatings();
			memberRatings.setMemberFeedback(feedbacks);
			if(rating != null){
				memberRatings.setRating(rating);
				if(rating >= 9 && rating <=10)
					memberRatings.setRatingLevel("Excellent");
				if(rating >= 7 && rating < 9)
					memberRatings.setRatingLevel("Good");
				if(rating >= 4 && rating <=6)
					memberRatings.setRatingLevel("Average");
				if(rating >= 1 && rating <=3)
					memberRatings.setRatingLevel("Poor");
				memberRatings.setRatingParameter(parameterName[i]);
				i++;
			}
			memberRatingList.add(memberRatings);
		}
		feedbacks.setRatings(memberRatingList);
		
		return feedbacks;
	}
	
	public List<MemberFeedbackVO> executeMapperForMemFeedbkView(List<MemberFeedback> resultList){
		List<MemberFeedbackVO> feedbackList = new ArrayList<>();
		for (MemberFeedback memberFeedback : resultList) {
			MemberFeedbackVO memFeedbackVO = new MemberFeedbackVO();
			memFeedbackVO.setFeedbackId(memberFeedback.getFeedbackId());
			memFeedbackVO.setFeedbackDate(memberFeedback.getFeedbackDate());
			memFeedbackVO.setFeedbackDetails(memberFeedback.getFeedbackDetails());
			memFeedbackVO.setMemberEmail(memberFeedback.getMemberEmail());
			memFeedbackVO.setMemberName(memberFeedback.getMemberName());
			memFeedbackVO.setMemberPhone(memberFeedback.getMemberPhone());
			Integer[] ratings = new Integer[9];
			String[] ratingLevel = new String[9];			
			String [] parameterNames =  new String[9];
			int index = 0;
			List<MemberFeedbackRatings> ratingsAndParameters = memberFeedback.getRatings();
			for (MemberFeedbackRatings memberFeedbackRatings : ratingsAndParameters) {
				parameterNames[index] = memberFeedbackRatings.getRatingParameter();
				ratings[index] = memberFeedbackRatings.getRating();
				ratingLevel[index] = memberFeedbackRatings.getRatingLevel();
				index++;
			}
			memFeedbackVO.setRatingLevel(ratingLevel);
			memFeedbackVO.setRatings(ratings);
			memFeedbackVO.setParameterNames(parameterNames);
			feedbackList.add(memFeedbackVO);
		}
		return feedbackList;
	}
	
	public List<MemberFeedbackVO> executeMapperForParameterBasedReport(List<MemberFeedback> resultList,String parameterCriteria){
		List<MemberFeedbackVO> memberFeedbackList = new ArrayList<>();
		for (MemberFeedback memberFeedback : resultList) {
			MemberFeedbackVO memFeedbackVO = new MemberFeedbackVO();
			memFeedbackVO.setFeedbackId(memberFeedback.getFeedbackId());
			memFeedbackVO.setFeedbackDate(memberFeedback.getFeedbackDate());
			memFeedbackVO.setFeedbackDetails(memberFeedback.getFeedbackDetails());
			memFeedbackVO.setMemberEmail(memberFeedback.getMemberEmail());
			memFeedbackVO.setMemberName(memberFeedback.getMemberName());
			memFeedbackVO.setMemberPhone(memberFeedback.getMemberPhone());
			Integer[] ratings = new Integer[9];
			String[] ratingLevel = new String[9];			
			String [] parameterNames =  new String[9];
			int index = 0;
			List<MemberFeedbackRatings> ratingsAndParameters = memberFeedback.getRatings();
			for (MemberFeedbackRatings memberFeedbackRatings : ratingsAndParameters) {
				
				if(memberFeedbackRatings.getRatingParameter().equals(parameterCriteria)){
					parameterNames[index] = memberFeedbackRatings.getRatingParameter();
					ratings[index] = memberFeedbackRatings.getRating();
					ratingLevel[index] = memberFeedbackRatings.getRatingLevel();
					index++;
				}
			}
			memFeedbackVO.setRatingLevel(ratingLevel);
			memFeedbackVO.setRatings(ratings);
			memFeedbackVO.setParameterNames(parameterNames);
			memberFeedbackList.add(memFeedbackVO);
		}
		
		return memberFeedbackList;
	}
	
	public List<MemberFeedbackVO> executeMapperForConsolidatedReport(List<MemberFeedback> resultList){
		List<MemberFeedbackVO> memberFeedbackList = new ArrayList<>();
		for (MemberFeedback memberFeedback : resultList) {
			MemberFeedbackVO memFeedbackVO = new MemberFeedbackVO();
			memFeedbackVO.setFeedbackId(memberFeedback.getFeedbackId());
			memFeedbackVO.setFeedbackDate(memberFeedback.getFeedbackDate());
			memFeedbackVO.setFeedbackDetails(memberFeedback.getFeedbackDetails());
			memFeedbackVO.setMemberEmail(memberFeedback.getMemberEmail());
			memFeedbackVO.setMemberName(memberFeedback.getMemberName());
			memFeedbackVO.setMemberPhone(memberFeedback.getMemberPhone());
			Integer[] ratings = new Integer[9];
			String[] ratingLevel = new String[9];			
			String [] parameterNames =  new String[9];
			int index = 0;
			List<MemberFeedbackRatings> ratingsAndParameters = memberFeedback.getRatings();
			for (MemberFeedbackRatings memberFeedbackRatings : ratingsAndParameters) {
				parameterNames[index] = memberFeedbackRatings.getRatingParameter();
				ratings[index] = memberFeedbackRatings.getRating();
				ratingLevel[index] = memberFeedbackRatings.getRatingLevel();
				index++;
			}
			memFeedbackVO.setRatingLevel(ratingLevel);
			memFeedbackVO.setRatings(ratings);
			memFeedbackVO.setParameterNames(parameterNames);
			memberFeedbackList.add(memFeedbackVO);
		}
		
		return memberFeedbackList;
	}
	
	
}

package com.danovus.connectpedia.admin.helper;

import java.util.ArrayList;
import java.util.List;

import com.danovus.cp.modules.helper.vo.NewUserVO;
import com.danovus.security.model.ExceptionFeatures;
import com.danovus.security.model.Gateway;
import com.danovus.security.model.UserFeatureAccess;

public class AdminUserCreationHelper {
	
	public Gateway executeMapper(NewUserVO newUser,String centreID){
		Gateway gateway = new Gateway();
		gateway.setCentreId(centreID);
		gateway.setPassword(newUser.getPassword());
		gateway.setRole(newUser.getUserType());
		gateway.setUserId(newUser.getUserName());
		return gateway;
	}
	
	public Gateway executeMapper(NewUserVO newUser, Gateway gateway, String centreID){
		gateway.setCentreId(centreID);
		//gateway.setPassword(newUser.getPassword());
		gateway.setRole(newUser.getUserType());
		//gateway.setUserId(newUser.getUserName());
		return gateway;
	}
	
	public UserFeatureAccess executeMapperForAccess(NewUserVO newUser){
		UserFeatureAccess userAccess = new UserFeatureAccess();
		userAccess.setUserId(newUser.getUserName());
		if(newUser.getAllFeatureAccess().equalsIgnoreCase("Y")){
			userAccess.setAcessToAllFeatures("Y");
			userAccess.setIsException("N");
		}else{
			userAccess.setAcessToAllFeatures("N");
			userAccess.setIsException("Y");
		}
		List<ExceptionFeatures> featureList = new ArrayList<ExceptionFeatures>();
		if(newUser.getFeaturesListEmployeeZone() != null){
			String[] featureSet = newUser.getFeaturesListEmployeeZone();
			for(int i=0; i<featureSet.length; i++){
				ExceptionFeatures featureUpdate = new ExceptionFeatures();
				featureUpdate.setFeatureName(featureSet[i]);
				featureUpdate.setUserFeatureAccess(userAccess);
				featureList.add(featureUpdate);
			}
		}
		userAccess.setExceptionFeatureId(featureList);
		
		return userAccess;
	}
	
	public NewUserVO executeMapperEntityToVo(Gateway newUser, UserFeatureAccess userAccess){
		NewUserVO newUserVO = new NewUserVO();
		
		newUserVO.setUserName(newUser.getUserId());
		newUserVO.setPassword(newUser.getPassword());
		newUserVO.setUserType(newUser.getRole());
		newUserVO.setAllFeatureAccess(userAccess.getAcessToAllFeatures());
		
		String[] exceptionFeature = new String[userAccess.getExceptionFeatureId().size()];
		int i= 0;
		for(ExceptionFeatures eachfeatures : userAccess.getExceptionFeatureId()){
			exceptionFeature[i] = eachfeatures.getFeatureName();
			i++;		
		}
		newUserVO.setFeaturesListEmployeeZone(exceptionFeature);
		return newUserVO;
	}

}

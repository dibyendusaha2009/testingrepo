package com.danovus.connectpedia.admin.helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jboss.security.auth.spi.Util;

import com.danovus.connectpedia.gyminfo.model.FitnessCentrePackagesAndProducts;
import com.danovus.connectpedia.gyminfo.model.FitnessCentrePackagesAndProductsCost;
import com.danovus.connectpedia.gyminfo.model.FitnessGroomingCentre;
import com.danovus.cp.framework.util.DateUtil;
import com.danovus.cp.modules.helper.common.vo.FitnessCentreVO;
import com.danovus.cp.modules.helper.common.vo.PackagesAndProductsVO;
import com.danovus.security.model.Gateway;
import com.danovus.security.model.UserFeatureAccess;

public class AdminCentreCreationHelper {
	
	public FitnessGroomingCentre executeMapper(FitnessCentreVO fitnessCentreVO){
		FitnessGroomingCentre newCentre = new FitnessGroomingCentre();
		
		newCentre.setCentreId(fitnessCentreVO.getCentreId());
		newCentre.setBranch(fitnessCentreVO.getBranch());
		newCentre.setCentreName(fitnessCentreVO.getCentreName());
		newCentre.setCentreOwnerName(fitnessCentreVO.getCentreOwnerName());
		newCentre.setCentreOwnerNum(fitnessCentreVO.getCentreOwnerNum());
		newCentre.setContactNum(fitnessCentreVO.getCentreContactNum());
		newCentre.setWebsite(fitnessCentreVO.getWebsite());
		
		
		newCentre.setStreet(fitnessCentreVO.getStreet());
		newCentre.setCity(fitnessCentreVO.getCity());
		newCentre.setState(fitnessCentreVO.getState());
		newCentre.setPostalCode(fitnessCentreVO.getPcode());
		newCentre.setEmail(fitnessCentreVO.getEmail());
		
		newCentre.setBillingEmail(fitnessCentreVO.getBillingEmail());
		newCentre.setIsBilling(fitnessCentreVO.getBilling());
		
		newCentre.setBioIdEqualsRcptId(fitnessCentreVO.getBioIdEqualsReceiptId());		
		newCentre.setBiometricDeviceId(fitnessCentreVO.getBioDeviceId());
		newCentre.setWebCam(fitnessCentreVO.getWebCam());
		
		newCentre.setClientType(fitnessCentreVO.getClientType());
		newCentre.setEnrollSMS(fitnessCentreVO.getEnrollSMS());
		newCentre.setEnquirySMS(fitnessCentreVO.getEnquirySMS());
		newCentre.setRenewSMS(fitnessCentreVO.getRenewSMS());
		newCentre.setSummarySMS(fitnessCentreVO.getSummarySMS());
		newCentre.setReminderSMS(fitnessCentreVO.getReminderSMS());
		newCentre.setSendSMSToTrainerOnEnrollment(fitnessCentreVO.getSendSMSToTrainer());
		newCentre.setSenderIdForSMS(fitnessCentreVO.getSmsSenderId());
		newCentre.setWorkingKey(fitnessCentreVO.getSmsWorkingKey());
		newCentre.setExpiredMembersAttendingSMS(fitnessCentreVO.getExpiredmembersAttending());

		newCentre.setDuplicateMobileAllowed(fitnessCentreVO.getDuplicateMobileAllowed());
		newCentre.setEnrollmentMailSenderId(fitnessCentreVO.getEnrollMailSenderId());
		newCentre.setEnrollmentMailSenderPWD(fitnessCentreVO.getEnrollMailSenderPwd());
		
		newCentre.setEnquiryToEnrollmentOnly(fitnessCentreVO.getEnquiryToEnrollment());
		newCentre.setOtpReceiverNum(fitnessCentreVO.getOtpReceiverNumber());		
		newCentre.setMultipleBranch(fitnessCentreVO.getMultipleBranch());
		
		return newCentre;
		
	}
	
	public List<FitnessCentreVO> retrievePackgeInValueObjectFormat(List<FitnessGroomingCentre> centreList){
		List<FitnessCentreVO> allCentre = new ArrayList<FitnessCentreVO>();
		for(FitnessGroomingCentre centre: centreList){
			FitnessCentreVO newCentre = new FitnessCentreVO();
			newCentre.setCentreId(centre.getCentreId());
			newCentre.setCentreName(centre.getCentreName());
			newCentre.setBranch(centre.getBranch());
			newCentre.setCentreContactNum(centre.getContactNum());
			newCentre.setEmail(centre.getEmail());
			newCentre.setOtpReceiverNumber(centre.getOtpReceiverNum());
			newCentre.setExpiryDate(centre.getExpiryDate());
			newCentre.setBillingEmail(centre.getBillingEmail());
			newCentre.setBioDeviceId(centre.getBiometricDeviceId());
			newCentre.setSmsSenderId(centre.getSenderIdForSMS());
			newCentre.setCentreOwnerNum(centre.getCentreOwnerNum());
			newCentre.setSmsWorkingKey(centre.getWorkingKey());
			newCentre.setLockedAccount(centre.getAccountLocked());
			allCentre.add(newCentre);
		}
		return allCentre;
	}
	
	public FitnessGroomingCentre executeMapperForUpdateCentre(FitnessGroomingCentre centre, FitnessCentreVO centreVo) {
		// TODO Auto-generated method stub
		centre.setCentreOwnerNum(centreVo.getCentreOwnerNum());
		centre.setContactNum(centreVo.getCentreContactNum());
		centre.setOtpReceiverNum(centreVo.getOtpReceiverNumber());
		centre.setEmail(centreVo.getEmail());
		if(centreVo.getExpiryDate() != null && !centreVo.getExpiryDate().isEmpty() && 
				!centreVo.getExpiryDate().contains("_")){
			centre.setExpiryDate(centreVo.getExpiryDate());
		}else
			centre.setExpiryDate(null);
		centre.setBillingEmail(centreVo.getBillingEmail());
		return centre;
	}
	
	public FitnessGroomingCentre executeMapperForLockCentre(FitnessGroomingCentre centre, FitnessCentreVO centreVo) {
		// TODO Auto-generated method stub
		centre.setAccountLocked("Y");
		centre.setAccountLockedMessage("Account_Blocked");
		centre.setWorkingKey(centreVo.getSmsWorkingKey().concat("BLOCKED"));
		return centre;
	}
	
	public FitnessGroomingCentre executeMapperForUnLockCentre(FitnessGroomingCentre centre, FitnessCentreVO centreVo) {
		// TODO Auto-generated method stub
		centre.setAccountLocked(null);
		centre.setAccountLockedMessage(null);
		String smsKeyBlocked = centreVo.getSmsWorkingKey();
		String smsKey = smsKeyBlocked.replaceAll("BLOCKED", "");
		centre.setWorkingKey(smsKey);
		return centre;
	}
	
	public Gateway executeMapper(FitnessCentreVO fitnessCentreVO,String centreId){
		Gateway gateway = new Gateway();
		gateway.setCentreId(centreId);
		gateway.setUserId(fitnessCentreVO.getUserId());
		gateway.setRole("super_user");
		gateway.setPassword(Util.createPasswordHash("MD5", "base64", null, null, fitnessCentreVO.getPassword()));
		return gateway;
	}
	
	public FitnessGroomingCentre executeMapperForAddNewCentre(FitnessCentreVO newCentreToAdd) {
		// TODO Auto-generated method stub
		FitnessGroomingCentre centre = new FitnessGroomingCentre();
		centre.setCentreId(newCentreToAdd.getCentreName().replace(" ", "_").toUpperCase());
		centre.setCentreName(newCentreToAdd.getCentreName());
		centre.setCentreOwnerNum(newCentreToAdd.getCentreOwnerNum());
		centre.setCentreOwnerName(newCentreToAdd.getCentreOwnerName());
		centre.setClientType(newCentreToAdd.getClientType());
		centre.setBranch(newCentreToAdd.getBranch());
		centre.setContactNum(newCentreToAdd.getCentreContactNum());
		centre.setEmail(newCentreToAdd.getEmail());
		
		if(!newCentreToAdd.getStreet().isEmpty() || newCentreToAdd.getStreet() != null){
		centre.setStreet(newCentreToAdd.getStreet());
		}else
		centre.setStreet(null);
		
		centre.setState(newCentreToAdd.getState());
		centre.setPostalCode(newCentreToAdd.getPcode());
		centre.setCity(newCentreToAdd.getCity());
		centre.setOtpReceiverNum(newCentreToAdd.getOtpReceiverNumber());
		centre.setBillingEmail(newCentreToAdd.getBillingEmail());
		centre.setEnrollmentMailRcvrId(newCentreToAdd.getEnrollMailReceiverId());
		centre.setCentreCreationDate(new Date());
	
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		int month = Integer.parseInt(newCentreToAdd.getClientMode()); 
		Calendar cal = Calendar.getInstance(); 
		cal.add(Calendar.MONTH, month);
		centre.setExpiryDate(simpleDateFormat.format(cal.getTime()));
		
		centre.setEnrollSMS("Y");
		centre.setEnquirySMS("Y");
		centre.setRenewSMS("Y");
		centre.setWorkingKey("b127143259cd3472a2f16e2125055532fd73f63d");
		centre.setSummarySMS("Y");
		centre.setIsBilling("Y");
		centre.setSenderIdForSMS("CPEDIA");
		centre.setSummaryMailSenderId("connectpedia@danovus.com");
		centre.setSummaryMailSenderPwd("DAnovus_05!");
		centre.setEnrollmentMailSenderId("connectpedia@danovus.com");
		centre.setEnrollmentMailSenderPWD("DAnovus_05!");
		centre.setEnquiryToEnrollmentOnly("N");
		centre.setSgst(9D);
		centre.setCgst(9D);
		centre.setAddPackageSms("Y");
		return centre;
	}
	
	public List<Gateway> executeMapperForGateway(FitnessCentreVO fitnessCentreVO,FitnessGroomingCentre newCentre) {
		// TODO Auto-generated method stub
		List<Gateway> gate = new ArrayList<>();
		for(int i = 1; i<4;i++){
			Gateway gateway = new Gateway();
			gateway.setCentreId(newCentre.getCentreId());
			gateway.setPassword("FNkG+x+G4mVFZRrX+4vV5w==");
			if(i==3){
				gateway.setUserId("da"+newCentre.getCentreName().substring(0, 4).toLowerCase() + newCentre.getCentreOwnerNum().substring(0, 3));
			}else{
				gateway.setUserId(newCentre.getCentreName().substring(0, 4).toLowerCase()+newCentre.getCentreOwnerNum().substring(0, 3)+"0"+ i);
			}
			
			gateway.setRole("super_user");
			gate.add(gateway);
		}
		return gate;
	}
	
	public List<UserFeatureAccess> executeMapperForUserFeatureAccess(List<Gateway> user) {
		// TODO Auto-generated method stub
		List<UserFeatureAccess> userFeatureAccessList = new ArrayList<>();
		for(Gateway centre: user){
			UserFeatureAccess userFeatureAccess = new UserFeatureAccess();
			userFeatureAccess.setAcessToAllFeatures("Y");
			userFeatureAccess.setIsException("N");
			userFeatureAccess.setUserId(centre.getUserId());
			userFeatureAccess.setExceptionFeatureId(null);
			userFeatureAccessList.add(userFeatureAccess);
		}
		return userFeatureAccessList;
	}
	
	public List<FitnessCentreVO> retrieveDaUsers(List<Gateway> user) {
		// TODO Auto-generated method stub
		List<FitnessCentreVO> daUsers = new ArrayList<>();
		for(Gateway centre: user){
			FitnessCentreVO centreVO = new FitnessCentreVO();
			centreVO.setCentreId(centre.getCentreId());
			centreVO.setDaUser(centre.getUserId());
			daUsers.add(centreVO);
		}
		return daUsers;
	}

}

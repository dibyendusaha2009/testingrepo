package com.danovus.connectpedia.admin.helper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.danovus.connectpedia.gyminfo.model.Ticket;
import com.danovus.cp.modules.helper.vo.TicketVO;

public class TicketHelper {
	
	public Ticket executeMapper(TicketVO ticketVO,String gymId){
		Ticket ticket = new Ticket();
		ticket.setCentreId(gymId);
		ticket.setTicketType(ticketVO.getTicketType());
		ticket.setModule(ticketVO.getModule());
		ticket.setPriority(ticketVO.getPriority());
		ticket.setCreatedDate(new Date());
		ticket.setDescription(ticketVO.getDescription());
		ticket.setStatus("OPEN");		
		ticket.setCreatdById(ticketVO.getCreatedById());
		return ticket;
	}
	
	public Ticket executeMapperForAssignUpdate(TicketVO ticketVO,String gymId,Ticket ticket){
		ticket.setCentreId(gymId);
		ticket.setTicketType(ticketVO.getTicketType());
		ticket.setModule(ticketVO.getModule());
		ticket.setPriority(ticketVO.getPriority());
		ticket.setCreatedDate(new Date());
		ticket.setDescription(ticketVO.getDescription());
		ticket.setStatus(ticketVO.getStatus());		
		ticket.setCreatdById(ticketVO.getCreatedById());
		ticket.setAssignedTo(ticketVO.getAssignedTo());
		ticket.setResolvedDate(ticketVO.getResolvedDate());
		ticket.setTargetEndDate(ticketVO.getTentativeDate());
		
		return ticket;
	}
	
	
	public List<TicketVO> executeMapperForView(List<Ticket> tickets){
		List<TicketVO> allTickets = new ArrayList<>();
		for(Ticket eachTicket : tickets){
			TicketVO ticketVO = new TicketVO();
			ticketVO.setTicketId(eachTicket.getTicketId());
			ticketVO.setDescription(eachTicket.getDescription());
			ticketVO.setCreatedById(eachTicket.getCreatdById());
			ticketVO.setPriority(eachTicket.getPriority());
			ticketVO.setModule(eachTicket.getModule());
			ticketVO.setCreatedDate(eachTicket.getCreatedDate());
			ticketVO.setTicketType(eachTicket.getTicketType());
			ticketVO.setTentativeDate(eachTicket.getTargetEndDate());
			ticketVO.setStatus(eachTicket.getStatus());
			ticketVO.setAssignedTo(eachTicket.getAssignedTo());
			ticketVO.setResolvedDate(eachTicket.getResolvedDate());
			ticketVO.setCentreId(eachTicket.getCentreId());
			allTickets.add(ticketVO);
			
		}
		return allTickets;
	}
}

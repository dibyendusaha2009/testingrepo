package com.danovus.connectpedia.admin.helper;

import java.util.Date;

import com.danovus.connectpedia.common.model.PackagePayment;
import com.danovus.connectpedia.enroll.model.Enrollment;
import com.danovus.connectpedia.gyminfo.model.CancelledReceipt;

public class CancelledReceiptHelper {
	
	public CancelledReceipt executeMapper(PackagePayment recordToRemove,String loggedInUser){
		CancelledReceipt cancelledReceipt = new CancelledReceipt();
		Enrollment enrollment  = recordToRemove.getEnrollment();
		cancelledReceipt.setMemberName(enrollment.getBasicDetails().getName()+ " "+enrollment.getBasicDetails().getLastName());
		cancelledReceipt.setContactNum(enrollment.getPersonalDetails().getContactNum());
		cancelledReceipt.setCentreId(enrollment.getFitnessCentre().getCentreId());
		if(recordToRemove.getReceipt() != null){
			cancelledReceipt.setReceiptId(recordToRemove.getReceipt().getReceiptId());
			cancelledReceipt.setGeneratedOn(recordToRemove.getReceipt().getGeneratedOn());
		}
		cancelledReceipt.setAmountIdCancelled(recordToRemove.getAmountId());
		cancelledReceipt.setActivationDate(recordToRemove.getActivationDate());
		cancelledReceipt.setAmountDue(recordToRemove.getAmountDue());
		cancelledReceipt.setAmountDueDate(recordToRemove.getAmountDueDate());
		cancelledReceipt.setAmountDuePymntDate(recordToRemove.getAmountDuePymntDate());
		cancelledReceipt.setAmountForDowngrade(recordToRemove.getAmountForDowngrade());
		cancelledReceipt.setAmountForUpgrade(recordToRemove.getAmountForUpgrade());
		cancelledReceipt.setAmountPaid(recordToRemove.getAmountPaid());
		cancelledReceipt.setAmountPaidInCard(recordToRemove.getAmountPaidInCard());
		cancelledReceipt.setAmountPaidInCash(recordToRemove.getAmountPaidInCash());
		cancelledReceipt.setAmountPaidInCheque(recordToRemove.getAmountPaidInCheque());
		cancelledReceipt.setAmountViaOnline(recordToRemove.getAmountViaOnline());
		cancelledReceipt.setBusinessBy(recordToRemove.getBusinessBy());
		cancelledReceipt.setCardApprCode(recordToRemove.getCardApprCode());
		cancelledReceipt.setCardNum(recordToRemove.getCardNum());
		cancelledReceipt.setChequeNum(recordToRemove.getChequeNum());
		cancelledReceipt.setClientRep(recordToRemove.getClientRep());
		cancelledReceipt.setDiscount(recordToRemove.getDiscount());
		cancelledReceipt.setDiscountType(recordToRemove.getDiscountType());
		cancelledReceipt.setDowngraded(recordToRemove.getDowngraded());
		cancelledReceipt.setDowngradeDate(recordToRemove.getDowngradeDate());
		cancelledReceipt.setEnrollmentDate(recordToRemove.getEnrollmentDate());
		cancelledReceipt.setExamFees(recordToRemove.getExamFees());
		cancelledReceipt.setIsAmountDueRemaning(recordToRemove.getIsAmountDueRemaning());
		cancelledReceipt.setManualReceiptNo(recordToRemove.getManualReceiptNo());
		cancelledReceipt.setMembershipMode(recordToRemove.getMembershipMode());
		cancelledReceipt.setMembExpiryDate(recordToRemove.getMembExpiryDate());
		cancelledReceipt.setMemebershipAmount(recordToRemove.getMemebershipAmount());
		cancelledReceipt.setNextFollowUpDate(recordToRemove.getNextFollowUpDate());
		cancelledReceipt.setNumOfSessionLeft(recordToRemove.getNumOfSessionLeft());
		cancelledReceipt.setOfferName(recordToRemove.getOfferName());
		cancelledReceipt.setOnlineMethod(recordToRemove.getOnlineMethod());
		cancelledReceipt.setPackageName(recordToRemove.getPackageName());
		cancelledReceipt.setPaidOn(recordToRemove.getPaidOn());
		cancelledReceipt.setPayableAmount(recordToRemove.getPayableAmount());
		cancelledReceipt.setPkgCreatedDate(recordToRemove.getPkgCreatedDate());
		cancelledReceipt.setPurpose(recordToRemove.getPurpose());		
		cancelledReceipt.setRegAmount(recordToRemove.getRegAmount());
		cancelledReceipt.setRemarks(recordToRemove.getRemarks());
		cancelledReceipt.setRenewed(recordToRemove.getRenewed());
		cancelledReceipt.setRenewalDate(recordToRemove.getRenewalDate());
		cancelledReceipt.setSelectedTax(recordToRemove.getSelectedTax());
		cancelledReceipt.setSessionPin(recordToRemove.getSessionPin());
		cancelledReceipt.setSpecialDiscountBy(recordToRemove.getSpecialDiscountBy());
		cancelledReceipt.setStarterKitPaidDate(recordToRemove.getStarterKitPaidDate());
		cancelledReceipt.setStartUpKit(recordToRemove.getStartUpKit());
		cancelledReceipt.setTaxInPercent(recordToRemove.getTaxInPercent());
		cancelledReceipt.setTotalAmount(recordToRemove.getTotalAmount());
		cancelledReceipt.setTrainerId(recordToRemove.getTrainerId());
		cancelledReceipt.setTrainerName(recordToRemove.getTrainerName());
		cancelledReceipt.setTransferCharge(recordToRemove.getTransferCharge());
		cancelledReceipt.setUpgradeAdjustment(recordToRemove.getUpgradeAdjustment());
		cancelledReceipt.setUpgraded(recordToRemove.getUpgraded());
		cancelledReceipt.setUpgradeDate(recordToRemove.getUpgradeDate());
		cancelledReceipt.setCancelledDate(new Date());
		cancelledReceipt.setCancelledBy(loggedInUser);
		return cancelledReceipt;
	}

}

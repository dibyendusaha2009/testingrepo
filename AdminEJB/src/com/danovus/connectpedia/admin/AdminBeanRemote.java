package com.danovus.connectpedia.admin;

import javax.ejb.Remote;

import com.danovus.cp.framework.data.ConnectPediaTO;

@Remote
public interface AdminBeanRemote {
	
	void sendOTPForDeletionToOwner(ConnectPediaTO requestResponseData);
	
	void deletePaymentRecordForMember(ConnectPediaTO requestResponseData);
	
	void createCentre(ConnectPediaTO requestResponseData);
	
	void deleteMemberProfile(ConnectPediaTO requestResponseData);
	
	void deleteBatch(ConnectPediaTO requestResponseData);
	
	void createNewUser(ConnectPediaTO requestResponseData);
	
	void sendOTPForStaffDeletionToOwner(ConnectPediaTO requestResponseData);
	
	void createTicket(ConnectPediaTO requestResponseData);
	
	void saveTODO(ConnectPediaTO requestResponseData);
	
	void updateToDo(ConnectPediaTO requestResponseData);
	
	void deleteStaffProfile(ConnectPediaTO requestResponseData);
	
	void generateAndSendUserDelOtp(ConnectPediaTO requestResponseData);
	
	void userDeletionProcess(ConnectPediaTO requestResponseData);
	
	void sendSummarySMSToOnwers(ConnectPediaTO requestResponseData);
	
	ConnectPediaTO addMemberFeedback(ConnectPediaTO requestResponseData);
	
	void addCentreBatchTiming(ConnectPediaTO requestResponseData);
	
	void configureGSTTax(ConnectPediaTO requestResponseData);
	
	ConnectPediaTO viewCentreBatchTiming(ConnectPediaTO requestResponseData);
	
	ConnectPediaTO validateUserIdExistence(ConnectPediaTO requestResponseData);
	
	ConnectPediaTO fetchAllUsersList(ConnectPediaTO requestResponseData);
	
	ConnectPediaTO fetchAllCentres(ConnectPediaTO requestResponseData);
	
	ConnectPediaTO fetchAllCentresDaUsers(ConnectPediaTO requestResponseData);
	
	ConnectPediaTO createNewCentres(ConnectPediaTO requestResponseData);

	ConnectPediaTO searchMembersForDietChartAddition(ConnectPediaTO requestRespData);
	
	void assignAndUpdateTicket(ConnectPediaTO requestRespData);
	
	public ConnectPediaTO retrieveSummarySMSDataForParticularDay(ConnectPediaTO requestData);
	
	public ConnectPediaTO viewTicket(ConnectPediaTO requestData);
	
	public ConnectPediaTO viewTodoList(ConnectPediaTO requestData);
	
	public ConnectPediaTO editExistingCentres(ConnectPediaTO requestData);
	
	public ConnectPediaTO lockCentre(ConnectPediaTO requestData);
	
	public ConnectPediaTO unlockCentre(ConnectPediaTO requestData);
	
	public ConnectPediaTO lockUser(ConnectPediaTO requestData);
	
	public ConnectPediaTO unlockUser(ConnectPediaTO requestData);
	 
	 ConnectPediaTO retrieveSummaryReminderSMSDataForParticularDay(ConnectPediaTO requestData);
	 
	 ConnectPediaTO getFeedbackByDateRange(ConnectPediaTO requestRespData);
		
	 ConnectPediaTO getAllFeedback(ConnectPediaTO requestRespData);
	 
	 ConnectPediaTO retrieveExistingMember(ConnectPediaTO requestRespData);
	 
	 ConnectPediaTO fetchReportBasedOnParameter(ConnectPediaTO requestRespData);
	 
	 ConnectPediaTO fetchConsolidatedReport(ConnectPediaTO requestRespData);

	ConnectPediaTO updateClientRepresentative(ConnectPediaTO requestData);
	
	ConnectPediaTO fetchUserAllAccess(ConnectPediaTO requestResponseData);

	void updateUserAccess(ConnectPediaTO requestData);
	 
//	 @Asynchronous
//	 void sendFeedbackToOwnerAndMember(String emailTo, MemberFeedbackVO user, FitnessGroomingCentre associatedGym );

	
}

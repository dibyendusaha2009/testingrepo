package com.danovus.connectpedia.admin.delegate;


import com.danovus.connectpedia.admin.AdminBeanRemote;
import com.danovus.cp.framework.data.ConnectPediaTO;
import com.danovus.cp.framework.delegate.CPDelegate;

public class AdminBD extends CPDelegate {
	
	public void invokeBusinessProcess(ConnectPediaTO requestData){
		String serviceNameAction = requestData.getAction();
		AdminBeanRemote adminBeanRemote = (AdminBeanRemote) getService(serviceNameAction,requestData.getModule());
		if(serviceNameAction.equals("deletePaymentInformation"))
			adminBeanRemote.deletePaymentRecordForMember(requestData);
		else if(serviceNameAction.equals("deletionOTP"))
			adminBeanRemote.sendOTPForDeletionToOwner(requestData);
		else if(serviceNameAction.equals("deletionOTPForStaff"))
			adminBeanRemote.sendOTPForStaffDeletionToOwner(requestData);
		else if(serviceNameAction.equals("deleteStaffProfile"))
			adminBeanRemote.deleteStaffProfile(requestData);
		else if(serviceNameAction.equals("deleteProfile"))
			adminBeanRemote.deleteMemberProfile(requestData);
		else if(serviceNameAction.equals("createNewUser"))
			adminBeanRemote.createNewUser(requestData);
		else if(serviceNameAction.equals("deletionOTPForUser"))
			adminBeanRemote.generateAndSendUserDelOtp(requestData);
		else if(serviceNameAction.equals("deletionUserProcess"))
			adminBeanRemote.userDeletionProcess(requestData);
		else if(serviceNameAction.equals("sendSMSToOnwers"))
			adminBeanRemote.sendSummarySMSToOnwers(requestData);
		else if(serviceNameAction.equals("createCentre"))
			adminBeanRemote.createCentre(requestData);
		else if(serviceNameAction.equals("storeBatchTime"))
			adminBeanRemote.addCentreBatchTiming(requestData);
		else if(serviceNameAction.equals("configTax"))
			adminBeanRemote.configureGSTTax(requestData);
		else if(serviceNameAction.equals("deleteBatch"))
			adminBeanRemote.deleteBatch(requestData);
		else if(serviceNameAction.equals("updateUserAccess"))
			adminBeanRemote.updateUserAccess(requestData);
		else if(serviceNameAction.equals("createTicket"))
			adminBeanRemote.createTicket(requestData);
		else if(serviceNameAction.equals("saveTODO"))
			adminBeanRemote.saveTODO(requestData);
		else if(serviceNameAction.equals("updateToDo"))
			adminBeanRemote.updateToDo(requestData);
		else if(serviceNameAction.equals("assignAndUpdateTicket")){
			adminBeanRemote.assignAndUpdateTicket(requestData);
		}
	}
	
	public ConnectPediaTO invokeBusinessProcess(ConnectPediaTO requestData,Boolean response){
		String serviceNameAction = requestData.getAction();
		AdminBeanRemote adminBeanRemote = (AdminBeanRemote) getService(serviceNameAction,requestData.getModule());
		if(serviceNameAction.equals("validateUserIdExistence"))
			return adminBeanRemote.validateUserIdExistence(requestData);
		else if(serviceNameAction.equals("getAllUsers"))
			return adminBeanRemote.fetchAllUsersList(requestData);
		else if(serviceNameAction.equals("viewDaUserForAllCentres"))
			return adminBeanRemote.fetchAllCentresDaUsers(requestData);
		else if(serviceNameAction.equals("viewExistingCentres"))
			return adminBeanRemote.fetchAllCentres(requestData);
		else if(serviceNameAction.equals("createNewCentres"))
			return adminBeanRemote.createNewCentres(requestData);
		else if(serviceNameAction.equals("summarySMSToOwners")){
			return adminBeanRemote.retrieveSummarySMSDataForParticularDay(requestData);
		}else if(serviceNameAction.equals("reminderSMSToOwners")){
			return adminBeanRemote.retrieveSummaryReminderSMSDataForParticularDay(requestData);
		}else if(serviceNameAction.equals("memForDietChartSearch"))
			return adminBeanRemote.searchMembersForDietChartAddition(requestData);
		else if(serviceNameAction.equals("feedbackByDateRange"))
			return adminBeanRemote.getFeedbackByDateRange(requestData);
		else if(serviceNameAction.equals("feedbackViewAll"))
			return adminBeanRemote.getAllFeedback(requestData);
		else if(serviceNameAction.equals("viewBatchTime"))
			return adminBeanRemote.viewCentreBatchTiming(requestData);
		else if(serviceNameAction.equals("retrieveExistingMember"))
			return adminBeanRemote.retrieveExistingMember(requestData);
		else if(serviceNameAction.equals("fetchReportBasedOnParamater"))
			return adminBeanRemote.fetchReportBasedOnParameter(requestData);
		else if(serviceNameAction.equals("fetchConsolidatedReport"))
			return adminBeanRemote.fetchConsolidatedReport(requestData);
		else if(serviceNameAction.equals("memFeedbackAdd"))
			return adminBeanRemote.addMemberFeedback(requestData);
		else if(serviceNameAction.equals("updateClientRep"))
			return adminBeanRemote.updateClientRepresentative(requestData);
		else if(serviceNameAction.equals("fetchUserAllAccess"))
			return adminBeanRemote.fetchUserAllAccess(requestData);
		else if(serviceNameAction.equals("viewTicket"))
			return adminBeanRemote.viewTicket(requestData);
		else if(serviceNameAction.equals("editExistingCentres"))
			return adminBeanRemote.editExistingCentres(requestData);
		else if(serviceNameAction.equals("lockCentre"))
			return adminBeanRemote.lockCentre(requestData);
		else if(serviceNameAction.equals("unlockCentre"))
			return adminBeanRemote.unlockCentre(requestData);
		else if(serviceNameAction.equals("lockUser"))
			return adminBeanRemote.lockUser(requestData);
		else if(serviceNameAction.equals("unlockUser"))
			return adminBeanRemote.unlockUser(requestData);
		else if(serviceNameAction.equals("viewTodoList"))
			return adminBeanRemote.viewTodoList(requestData);
		return null;
	}
	
	

}

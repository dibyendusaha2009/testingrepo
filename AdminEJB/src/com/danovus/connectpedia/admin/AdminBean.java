package com.danovus.connectpedia.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import com.danovus.connectpedia.admin.helper.AdminCentreCreationHelper;
import com.danovus.connectpedia.admin.helper.AdminQueryHelper;
import com.danovus.connectpedia.admin.helper.AdminUserCreationHelper;
import com.danovus.connectpedia.admin.helper.BatchTimeHelper;
import com.danovus.connectpedia.admin.helper.CancelledReceiptHelper;
import com.danovus.connectpedia.admin.helper.DietChartHelper;
import com.danovus.connectpedia.admin.helper.MemFeedbackHelper;
import com.danovus.connectpedia.admin.helper.TicketHelper;
import com.danovus.connectpedia.admin.helper.TodoListHelper;
import com.danovus.connectpedia.common.model.MemberFeedback;
import com.danovus.connectpedia.common.model.PackagePayment;
import com.danovus.connectpedia.common.model.TodoList;
import com.danovus.connectpedia.enquiry.model.Enquiry;
import com.danovus.connectpedia.enquiry.model.EnquiryFollowUp;
import com.danovus.connectpedia.enroll.model.Enrollment;
import com.danovus.connectpedia.gyminfo.model.CancelledReceipt;
import com.danovus.connectpedia.gyminfo.model.CustomBatches;
import com.danovus.connectpedia.gyminfo.model.Employee;
import com.danovus.connectpedia.gyminfo.model.FitnessCentrePackagesAndProducts;
import com.danovus.connectpedia.gyminfo.model.FitnessCentrePackagesAndProductsCost;
import com.danovus.connectpedia.gyminfo.model.FitnessGroomingCentre;
import com.danovus.connectpedia.gyminfo.model.LastReceiptId;
import com.danovus.connectpedia.gyminfo.model.Ticket;
import com.danovus.connectpedia.utility.MailHelper;
import com.danovus.connectpedia.utility.TriggerSMS;
import com.danovus.cp.framework.data.ConnectPediaTO;
import com.danovus.cp.framework.exceptions.InvalidDestinationNumberException;
import com.danovus.cp.framework.util.DateUtil;
import com.danovus.cp.modules.helper.common.vo.CustomBatchesVO;
import com.danovus.cp.modules.helper.common.vo.EnrollmentPackageInfoVO;
import com.danovus.cp.modules.helper.common.vo.FitnessCentreVO;
import com.danovus.cp.modules.helper.common.vo.MemberFeedbackVO;
import com.danovus.cp.modules.helper.common.vo.PackagesAndProductsVO;
import com.danovus.cp.modules.helper.common.vo.TaxConfigVO;
import com.danovus.cp.modules.helper.common.vo.TodoListVO;
import com.danovus.cp.modules.helper.vo.FeedbackReportVO;
import com.danovus.cp.modules.helper.vo.NewUserVO;
import com.danovus.cp.modules.helper.vo.Staff;
import com.danovus.cp.modules.helper.vo.TicketVO;
import com.danovus.cp.modules.helper.vo.User;
import com.danovus.security.model.ExceptionFeatures;
import com.danovus.security.model.Gateway;
import com.danovus.security.model.UserFeatureAccess;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

/**
 * Session Bean implementation class AdminBean
 */

@Stateless(name="adminBean", mappedName = "adminBean")
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class AdminBean implements AdminBeanRemote {

	@PersistenceContext(unitName="GYM_CONNECT")
	private EntityManager em;

	@Resource
	private SessionContext ssnCntxt;
    /**
     * Default constructor. 
     */
	public AdminBean() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void deletePaymentRecordForMember(ConnectPediaTO requestResponseData) {
		Long parentAmountId = -1L;
		Long amountId = (Long)requestResponseData.getUserRequest();
		PackagePayment recordToRemove = em.find(PackagePayment.class, amountId);
		parentAmountId = recordToRemove.getParentAmountId();
		if(parentAmountId != null && parentAmountId != -1L){
			PackagePayment recordToUpdate = em.find(PackagePayment.class, parentAmountId);
			if(recordToRemove.getPurpose() != null && recordToRemove.getPurpose().equals("RENEWAL")){
				recordToUpdate.setRenewed(null);
			}
			if(recordToRemove.getPurpose() != null && recordToRemove.getPurpose().equals("AMOUNT_DUE_PAYMENT")){
				recordToUpdate.setIsAmountDueRemaning("Y");
			}
			if(recordToRemove.getPurpose() != null && recordToRemove.getPurpose().contains("UPGRAD")){
				recordToUpdate.setUpgraded(null);
			}
			em.persist(recordToUpdate);
		}
		CancelledReceipt cancelledReceipt = new CancelledReceiptHelper().executeMapper(recordToRemove,requestResponseData.getLoggedInUser());
		em.persist(cancelledReceipt);
		em.remove(recordToRemove);
	}
	
	@Override
	public void sendOTPForStaffDeletionToOwner(ConnectPediaTO requestResponseData) {
		MailHelper mailer = null;
		Staff selectedStaff = (Staff)requestResponseData.getUserRequest();
		String template = "Dear Owner, Please find the OTP "+selectedStaff.getId()+" For Deletion of Staff Profile : "+selectedStaff.getPerson().getFirstName()+" "+selectedStaff.getPerson().getLastName();
		FitnessGroomingCentre deleteForCentre = em.find(FitnessGroomingCentre.class, requestResponseData.getGym());
		String[] emailTo = formEmailToList(deleteForCentre.getEmail());
		mailer = new MailHelper(emailTo,"","OTP for Staff Profile Deletion ",template,deleteForCentre.getEnrollmentMailSenderId(),deleteForCentre.getEnrollmentMailSenderPWD(),"");
//		List<String> contactNumberOfOwners = Arrays.asList(deleteForCentre.getCentreOwnerNum().split(","));
		List<String> contactNumberOfOwners = Arrays.asList(deleteForCentre.getOtpReceiverNum().split(","));
		TriggerSMS triggerSMS = new TriggerSMS(contactNumberOfOwners, template, deleteForCentre.getWorkingKey(), deleteForCentre.getSenderIdForSMS());
		try{
			triggerSMS.triggerSMS(template);
		}catch(InvalidDestinationNumberException iDNExc){
			iDNExc.printStackTrace();
		}
		try{
			mailer.sendEmail();
		}catch(Exception iDNExc){
			iDNExc.printStackTrace();
		}
	}
	

	@Override
	public void sendOTPForDeletionToOwner(ConnectPediaTO requestResponseData) {
		User selectedUser = null;
		EnrollmentPackageInfoVO paymentInfo = null;
		String template = null;
		MailHelper mailer = null;
		
		FitnessGroomingCentre deleteForCentre = em.find(FitnessGroomingCentre.class, requestResponseData.getGym());
		
		String[] emailTo = formEmailToList(deleteForCentre.getEmail());
		
		if(requestResponseData.getUserRequest() instanceof EnrollmentPackageInfoVO){
			paymentInfo = (EnrollmentPackageInfoVO)requestResponseData.getUserRequest();
			template = "Dear Owner, Please find the OTP "+paymentInfo.getId()+" For Deletion of Package : "+paymentInfo.getPackageName();
			mailer = new MailHelper(emailTo,"","OTP for Package Deletion ",template,deleteForCentre.getEnrollmentMailSenderId(),deleteForCentre.getEnrollmentMailSenderPWD(),"");
		}else{
			selectedUser = (User)requestResponseData.getUserRequest();
			template = "Dear Owner, Please find the OTP "+selectedUser.getId()+" For Deletion of User : "+selectedUser.getPerson().getFirstName()+" "+selectedUser.getPerson().getLastName();
			mailer = new MailHelper(emailTo,"","OTP for Member Deletion  ",template,deleteForCentre.getEnrollmentMailSenderId(),deleteForCentre.getEnrollmentMailSenderPWD(),"");
		}
		
//		List<String> contactNumberOfOwners = Arrays.asList(deleteForCentre.getCentreOwnerNum().split(","));
		List<String> contactNumberOfOwners = Arrays.asList(deleteForCentre.getOtpReceiverNum().split(","));
		TriggerSMS triggerSMS = new TriggerSMS(contactNumberOfOwners, template, deleteForCentre.getWorkingKey(), deleteForCentre.getSenderIdForSMS());
		try{
			triggerSMS.triggerSMS(template);
		}catch(InvalidDestinationNumberException iDNExc){
			iDNExc.printStackTrace();
		}
		try{
			mailer.sendEmail();
		}catch(Exception iDNExc){
			iDNExc.printStackTrace();
		}
	}
	
	@Override
	public void deleteStaffProfile(ConnectPediaTO requestResponseData) {
		Long staffId = (Long)requestResponseData.getUserRequest();
		Employee profile = em.find(Employee.class, staffId);
		em.remove(profile);
	}
	

	@Override
	public void deleteMemberProfile(ConnectPediaTO requestResponseData) {
		Long enrollId = (Long)requestResponseData.getUserRequest();
		Enrollment profile = em.find(Enrollment.class, enrollId);
		em.remove(profile);
	}

	@Override
	public void createNewUser(ConnectPediaTO requestResponseData) {
		NewUserVO newUserVO = (NewUserVO)requestResponseData.getUserRequest();
		String gymId = requestResponseData.getGym();
		AdminUserCreationHelper userCreationHelper = new AdminUserCreationHelper();
		Gateway gateway = userCreationHelper.executeMapper(newUserVO, gymId);
			em.persist(gateway);
			UserFeatureAccess userFeatureAccess = new UserFeatureAccess();
			if(newUserVO.getAllFeatureAccess().equalsIgnoreCase("Y")){
				userFeatureAccess.setAcessToAllFeatures("Y");
				userFeatureAccess.setIsException("N");
				userFeatureAccess.setUserId(newUserVO.getUserName());
				userFeatureAccess.setExceptionFeatureId(null);
			}else{
				userFeatureAccess.setAcessToAllFeatures("N");
				userFeatureAccess.setIsException("Y");
				userFeatureAccess.setUserId(newUserVO.getUserName());
				userFeatureAccess.setExceptionFeatureId(null);
				List<ExceptionFeatures> exceptionFeatureList = new ArrayList<>();
				if(newUserVO.getFeaturesListEmployeeZone() != null){
					for(String featureName:newUserVO.getFeaturesListEmployeeZone()){
						ExceptionFeatures exceptionFeatures = new ExceptionFeatures();
						exceptionFeatures.setFeatureName(featureName);
						exceptionFeatures.setUserFeatureAccess(userFeatureAccess);
						exceptionFeatureList.add(exceptionFeatures);
						em.persist(exceptionFeatures);
					}			
				}
				if(newUserVO.getFeaturesListOwnerZone() != null){
					for(String featureName:newUserVO.getFeaturesListOwnerZone()){
						ExceptionFeatures exceptionFeatures = new ExceptionFeatures();
						exceptionFeatures.setFeatureName(featureName);
						exceptionFeatures.setUserFeatureAccess(userFeatureAccess);
						exceptionFeatureList.add(exceptionFeatures);
						em.persist(exceptionFeatures);
					}
				}
				if(newUserVO.getFeaturesListMarketingZone() != null){
					for(String featureName:newUserVO.getFeaturesListMarketingZone()){
						ExceptionFeatures exceptionFeatures = new ExceptionFeatures();
						exceptionFeatures.setFeatureName(featureName);
						exceptionFeatures.setUserFeatureAccess(userFeatureAccess);
						exceptionFeatureList.add(exceptionFeatures);
						em.persist(exceptionFeatures);
					}
				}
				if(newUserVO.getFeaturesListReportZone() != null){
					for(String featureName:newUserVO.getFeaturesListReportZone()){
						ExceptionFeatures exceptionFeatures = new ExceptionFeatures();
						exceptionFeatures.setFeatureName(featureName);
						exceptionFeatures.setUserFeatureAccess(userFeatureAccess);
						exceptionFeatureList.add(exceptionFeatures);
						em.persist(exceptionFeatures);
					}
				}
				userFeatureAccess.setExceptionFeatureId(exceptionFeatureList);
			}
			em.persist(userFeatureAccess);
	}
	
	@Override
	public ConnectPediaTO validateUserIdExistence(
			ConnectPediaTO requestResponseData) {
		Gateway gateway = em.find(Gateway.class,(String)requestResponseData.getUserRequest());
		if(gateway == null)
			requestResponseData.setAppResponse(Boolean.TRUE);
		else
			requestResponseData.setAppResponse(Boolean.FALSE);
		return requestResponseData;
	}

	@Override
	public ConnectPediaTO fetchAllUsersList(ConnectPediaTO requestResponseData) {
	
		Query queryfetchAllUsers = em.createNativeQuery(AdminQueryHelper.FETCH_ALL_USERS);
		queryfetchAllUsers.setParameter(1, requestResponseData.getGym());
		List<Object[]> userLoginDtls = (List<Object[]>) queryfetchAllUsers.getResultList();
		requestResponseData.setAppResponse(userLoginDtls);
		return requestResponseData;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ConnectPediaTO fetchAllCentres(ConnectPediaTO requestResponseData) {
	
		AdminCentreCreationHelper adminCreationHelper = new AdminCentreCreationHelper();
		Query findAllCentres = em.createNamedQuery("FitnessGroomingCentre.findAllCentres");
		List<FitnessGroomingCentre> centreList = (List<FitnessGroomingCentre>) findAllCentres.getResultList();
		List<FitnessCentreVO> productList = adminCreationHelper.retrievePackgeInValueObjectFormat(centreList);
		requestResponseData.setAppResponselist(productList);
		return requestResponseData;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ConnectPediaTO fetchAllCentresDaUsers(ConnectPediaTO requestResponseData) {
		 String userReq = (String) requestResponseData.getUserRequest();
		 Query findCentre = em.createNamedQuery("Gateway.getCentres");
		 findCentre.setParameter("centreId", "%"+userReq+"%");			
		 List<Gateway> userList = (List<Gateway>) findCentre.getResultList();
		 AdminCentreCreationHelper adminCreationHelper = new AdminCentreCreationHelper();
		 List<FitnessCentreVO> daUserList = adminCreationHelper.retrieveDaUsers(userList);
		 requestResponseData.setAppResponselist(daUserList);
		 return requestResponseData;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ConnectPediaTO createNewCentres(ConnectPediaTO requestResponseData) {
	
		FitnessCentreVO fitnessCentreVO = (FitnessCentreVO)requestResponseData.getUserRequest();
		AdminCentreCreationHelper adminCreationHelper = new AdminCentreCreationHelper();
		FitnessGroomingCentre newCentre = adminCreationHelper.executeMapperForAddNewCentre(fitnessCentreVO);
		em.persist(newCentre);
		
		List<Gateway> gateway = adminCreationHelper.executeMapperForGateway(fitnessCentreVO, newCentre);
		for(Gateway newUser: gateway){
			em.persist(newUser);
		}
		
		List<UserFeatureAccess> userFeatureAccess = adminCreationHelper.executeMapperForUserFeatureAccess(gateway);
		for(UserFeatureAccess userAccess: userFeatureAccess)
		em.persist(userAccess);
		
		
		LastReceiptId lastReceiptId = new LastReceiptId();
		lastReceiptId.setCentreId(newCentre.getCentreId());
		lastReceiptId.setLastReceiptId(0L);
		em.persist(lastReceiptId);
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ConnectPediaTO editExistingCentres(ConnectPediaTO requestRespData) {
		// TODO Auto-generated method stub
		AdminCentreCreationHelper adminCreationHelper = new AdminCentreCreationHelper();
		FitnessCentreVO selectedCentreToAdd = (FitnessCentreVO)requestRespData.getUserRequest();
		FitnessGroomingCentre fitCentre = retrieveCentreId(selectedCentreToAdd.getCentreId());
		FitnessGroomingCentre updatedCentre = new AdminCentreCreationHelper().executeMapperForUpdateCentre(fitCentre,selectedCentreToAdd);
		em.persist(updatedCentre);
		Query findAllCentres = em.createNamedQuery("FitnessGroomingCentre.findAllCentres");
		List<FitnessGroomingCentre> centreList = (List<FitnessGroomingCentre>) findAllCentres.getResultList();
		List<FitnessCentreVO> productList = adminCreationHelper.retrievePackgeInValueObjectFormat(centreList);
		requestRespData.setAppResponselist(productList);
		return requestRespData;
	}
	
	@Override
	public ConnectPediaTO lockCentre(ConnectPediaTO requestRespData) {
		// TODO Auto-generated method stub
		AdminCentreCreationHelper adminCreationHelper = new AdminCentreCreationHelper();
		FitnessCentreVO selectedCentreToAdd = (FitnessCentreVO)requestRespData.getUserRequest();
		FitnessGroomingCentre fitCentre = retrieveCentreId(selectedCentreToAdd.getCentreId());
		FitnessGroomingCentre updatedCentre = new AdminCentreCreationHelper().executeMapperForLockCentre(fitCentre,selectedCentreToAdd);
		em.persist(updatedCentre);
		Query findAllCentres = em.createNamedQuery("FitnessGroomingCentre.findAllCentres");
		List<FitnessGroomingCentre> centreList = (List<FitnessGroomingCentre>) findAllCentres.getResultList();
		List<FitnessCentreVO> productList = adminCreationHelper.retrievePackgeInValueObjectFormat(centreList);
		requestRespData.setAppResponselist(productList);
		return requestRespData;
	}
	
	@Override
	public ConnectPediaTO unlockCentre(ConnectPediaTO requestRespData) {
		// TODO Auto-generated method stub
		AdminCentreCreationHelper adminCreationHelper = new AdminCentreCreationHelper();
		FitnessCentreVO selectedCentreToAdd = (FitnessCentreVO)requestRespData.getUserRequest();
		FitnessGroomingCentre fitCentre = retrieveCentreId(selectedCentreToAdd.getCentreId());
		FitnessGroomingCentre updatedCentre = new AdminCentreCreationHelper().executeMapperForUnLockCentre(fitCentre,selectedCentreToAdd);
		em.persist(updatedCentre);
		Query findAllCentres = em.createNamedQuery("FitnessGroomingCentre.findAllCentres");
		List<FitnessGroomingCentre> centreList = (List<FitnessGroomingCentre>) findAllCentres.getResultList();
		List<FitnessCentreVO> productList = adminCreationHelper.retrievePackgeInValueObjectFormat(centreList);
		requestRespData.setAppResponselist(productList);
		return requestRespData;
	}
	
	@Override
	public ConnectPediaTO lockUser(ConnectPediaTO requestRespData) {
		// TODO Auto-generated method stub
		String userId = (String)requestRespData.getUserRequest();
		Gateway userList = retrieveUser(userId);
		userList.setIsOtpEnabled("Y");
		em.persist(userList);
		return requestRespData;
	}
	
	@Override
	public ConnectPediaTO unlockUser(ConnectPediaTO requestRespData) {
		// TODO Auto-generated method stub
		String userId = (String)requestRespData.getUserRequest();
		Gateway userList = retrieveUser(userId);
		userList.setIsOtpEnabled(null);
		em.persist(userList);
		return requestRespData;
	}
	
	private Gateway retrieveUser(String userId){
		 Query findUser = em.createNamedQuery("Gateway.getUser");
		 findUser.setParameter("userId", userId);
		 List<Gateway> userList =  (List<Gateway>) findUser.getResultList();
		 return userList.get(0);
	 }
	
	private FitnessGroomingCentre retrieveCentreId(String centreId){
		 Query findEntityByProductCostKeyQuery = em.createNamedQuery("FitnessGroomingCentre.findCentreKey");
		 findEntityByProductCostKeyQuery.setParameter("centreId", centreId);			
		 List<FitnessGroomingCentre> packageAndProductCostList = (List<FitnessGroomingCentre>) findEntityByProductCostKeyQuery.getResultList();
		 if(packageAndProductCostList != null && !packageAndProductCostList.isEmpty())
			return packageAndProductCostList.get(0);
		 return null;
	 }
	
	@Override
	public void generateAndSendUserDelOtp(ConnectPediaTO requestResponseData) {
	
		Object[] selectedUser = (Object[])requestResponseData.getUserRequest();
		char charFirst = selectedUser[0].toString().charAt(0);    
		char charScnd = selectedUser[0].toString().charAt(1);    
		int asciiValFrst = (int) charFirst;
		int asciiValScnd = (int) charScnd;
		int otpValue = asciiValFrst + asciiValScnd;
		MailHelper mailer = null;
		String template = "Dear Owner, Please find the OTP "+otpValue+" For Deletion of User";
		FitnessGroomingCentre deleteForCentre = em.find(FitnessGroomingCentre.class, requestResponseData.getGym());
		String[] emailTo = formEmailToList(deleteForCentre.getEmail());
		mailer = new MailHelper(emailTo,"","OTP for User Deletion ",template,deleteForCentre.getEnrollmentMailSenderId(),deleteForCentre.getEnrollmentMailSenderPWD(),"");
//		List<String> contactNumberOfOwners = Arrays.asList(deleteForCentre.getCentreOwnerNum().split(","));
		List<String> contactNumberOfOwners = Arrays.asList(deleteForCentre.getOtpReceiverNum().split(","));
		TriggerSMS triggerSMS = new TriggerSMS(contactNumberOfOwners, template, deleteForCentre.getWorkingKey(), deleteForCentre.getSenderIdForSMS());
		try{
			triggerSMS.triggerSMS(template);
		}catch(InvalidDestinationNumberException iDNExc){
			iDNExc.printStackTrace();
		}
		try{
			mailer.sendEmail();
		}catch(Exception iDNExc){
			iDNExc.printStackTrace();
		}
	}
	
	@Override
	public void userDeletionProcess(ConnectPediaTO requestResponseData) {
	
		Object[] reqData = (Object[])requestResponseData.getUserRequest();
		String userToDel = reqData[0].toString();
		Query queryToDelFrmExceptionFtr = em.createNativeQuery(AdminQueryHelper.DELETE_FROM_EXCEPTION_FEATURE);
		queryToDelFrmExceptionFtr.setParameter(1, userToDel);
		queryToDelFrmExceptionFtr.executeUpdate();
		
		Query queryToDelFrmFtrAccess = em.createNativeQuery(AdminQueryHelper.DELETE_FROM_FEATURE_ACCESS);
		queryToDelFrmFtrAccess.setParameter(1, userToDel);
		queryToDelFrmFtrAccess.executeUpdate();
		
		Query queryToDelFrmGateway = em.createNativeQuery(AdminQueryHelper.DELETE_FROM_GATEWAY);
		queryToDelFrmGateway.setParameter(1, userToDel);
		queryToDelFrmGateway.executeUpdate();

	}
	
	@Override
    public ConnectPediaTO retrieveSummarySMSDataForParticularDay(ConnectPediaTO requestData){
    	FitnessGroomingCentre eachGym = em.find(FitnessGroomingCentre.class, requestData.getGym());
    	if(eachGym.getCentreOwnerNum() != null && !eachGym.getCentreOwnerNum().isEmpty() 
				&& eachGym.getSummarySMS()!= null && eachGym.getSummarySMS().equalsIgnoreCase("Y")){
    		String gymId = requestData.getGym();
    		String gymName = eachGym.getCentreName();
    		Date userSelectedDate = (Date)requestData.getUserRequest();
    		String totalIncomeSummary = getTotalIncomeSummary(gymId,userSelectedDate) != null? getTotalIncomeSummary(gymId,userSelectedDate).toString():"0";
    		String totalEnrollmentCount = getTotalEnrollmentCount(gymId,userSelectedDate) != null? getTotalEnrollmentCount(gymId,userSelectedDate).toString():"0";
    		String renewalCount = getTotalRenewalCount(gymId,userSelectedDate) != null? getTotalRenewalCount(gymId,userSelectedDate).toString():"0";
    		String totalAmntDuePaid =  getTotalAmntDuePaid(gymId,userSelectedDate) != null ? getTotalAmntDuePaid(gymId,userSelectedDate).toString():"0";
    		String totalSalesTillDateForThisMonth = getTotalSalesTillDateForThisMonth(gymId,userSelectedDate) != null ?getTotalSalesTillDateForThisMonth(gymId,userSelectedDate).toString():"0";
    		String totalExpenseTillDateForThisMonth = getTotalExpenseTillDateForThisMonth(gymId,userSelectedDate).toString();
    		Double profitForThisMonth = (Double.parseDouble(totalSalesTillDateForThisMonth) - Double.parseDouble(totalExpenseTillDateForThisMonth)); 
    		String totalProfitTillDateForThisMonth = profitForThisMonth.toString();
    		String memberFootfall = getMembersFootfalls(eachGym.getBiometricDeviceId(),userSelectedDate);
    		String staffFootfall = getStaffFootfalls(eachGym.getBiometricDeviceId(),userSelectedDate);
    		String totalCashPayment = getCashCardPayment(gymId,userSelectedDate)[0].toString();
    		String totalCardPayment = getCashCardPayment(gymId,userSelectedDate)[1].toString();
    		String totalChqPayment = getCashCardPayment(gymId,userSelectedDate)[2].toString();
    		String template = createSMSTemplate(gymName,totalIncomeSummary,totalEnrollmentCount,renewalCount,totalAmntDuePaid,totalSalesTillDateForThisMonth, memberFootfall, staffFootfall,totalCashPayment,totalCardPayment,totalChqPayment, totalExpenseTillDateForThisMonth, totalProfitTillDateForThisMonth,userSelectedDate);
    		requestData.setAppResponse(template);
        	return requestData;
    	}
    	return null;
		
    }
    	
    @Override
	public ConnectPediaTO retrieveSummaryReminderSMSDataForParticularDay(ConnectPediaTO requestData){
		FitnessGroomingCentre eachGym = em.find(FitnessGroomingCentre.class, requestData.getGym());
		if(eachGym.getCentreOwnerNum() != null && !eachGym.getCentreOwnerNum().isEmpty() 
				&& eachGym.getSummarySMS()!= null && eachGym.getSummarySMS().equalsIgnoreCase("Y")){
			String gymId = requestData.getGym();		
			Date userSelectedDate = (Date)requestData.getUserRequest();
			String gymName = eachGym.getCentreName();
			Integer tomorrowsAmountDueCount = getAmountDueSummary(gymId,userSelectedDate).size();
			Integer tomorrowsEnqListCount = getEnqListSummary(gymId,userSelectedDate).size();
			Integer tomorrowsExpiryListCount = getExpiryListSummary(gymId,userSelectedDate).size();
			String template = createSMSTemplateForTomorrowsReminder(gymName,tomorrowsAmountDueCount,tomorrowsExpiryListCount,tomorrowsEnqListCount);
			requestData.setAppResponse(template);
	    	return requestData;
		}
		return null;
		
	}
        	
	private String getStaffFootfalls(String biometricDeviceId,Date userSelectedDateForSMS) {
		// TODO Auto-generated method stub
    	if(biometricDeviceId!=null){
    	Query query = em.createNativeQuery(AdminQueryHelper.STAFF_FOOTFALL);
		query.setParameter(1, biometricDeviceId);
		query.setParameter(2, userSelectedDateForSMS,TemporalType.DATE);
		query.setParameter(3, biometricDeviceId);
		return query.getSingleResult().toString();
    	}
    	else
    		return "N/A";
	}

	private String getMembersFootfalls(String biometricDeviceId,Date userSelectedDateForSMS) {
		// TODO Auto-generated method stub
		if(biometricDeviceId!=null){
		    Query todayMemFootfl = em.createNativeQuery(AdminQueryHelper.MEMBER_FOOTFALL);
	    	todayMemFootfl.setParameter(1, biometricDeviceId);	    
	    	todayMemFootfl.setParameter(2, userSelectedDateForSMS,TemporalType.DATE);	    
	    	return todayMemFootfl.getSingleResult().toString();
		}
		else
			return "N/A";
	}

    
    @SuppressWarnings("unchecked")
    private List<Object[]> getAmountDueSummary(String gymId,Date userSelectedDateForSMS){
    	Query amountDueQuery = em.createNativeQuery(AdminQueryHelper.SUMMARY_MAIL_AMOUNT_DUE_CLAUSE);
    	amountDueQuery.setParameter(1, gymId);
    	amountDueQuery.setParameter(2, userSelectedDateForSMS,TemporalType.DATE);
		return amountDueQuery.getResultList();
    }
    
    @SuppressWarnings("unchecked")
    private List<Object[]> getEnqListSummary(String gymId,Date userSelectedDateForSMS){
    	Query enqListQuery = em.createNativeQuery(AdminQueryHelper.SUMMARY_MAIL_ENQUIRY_FLW_CLAUSE);
    	enqListQuery.setParameter(1, gymId);
    	enqListQuery.setParameter(2, userSelectedDateForSMS,TemporalType.DATE);
		return enqListQuery.getResultList();
    }
    @SuppressWarnings("unchecked")
    private List<Object[]> getExpiryListSummary(String gymId,Date userSelectedDateForSMS){
    	Query expiryListQuery = em.createNativeQuery(AdminQueryHelper.SUMMARY_MAIL_EXPIRY_WHERE_CLAUSE);
    	expiryListQuery.setParameter(1, gymId);
    	expiryListQuery.setParameter(2, userSelectedDateForSMS,TemporalType.DATE);
		return expiryListQuery.getResultList();
    }
    
    private Object getTotalIncomeSummary(String gymId,Date userSelectedDateForSMS){
    	Query totalIncome = em.createNativeQuery(AdminQueryHelper.SUMMARY_MAIL_TOTAL_INCOME);
    	totalIncome.setParameter(1, gymId);
    	totalIncome.setParameter(2, userSelectedDateForSMS,TemporalType.DATE);
    	totalIncome.setParameter(3, userSelectedDateForSMS,TemporalType.DATE);
    	totalIncome.setParameter(4, userSelectedDateForSMS,TemporalType.DATE);
		return totalIncome.getSingleResult();
    }
    
    private Object getTotalEnrollmentCount(String gymId,Date userSelectedDateForSMS){
    	Query totalIncome = em.createNativeQuery(AdminQueryHelper.ENROLLMENT_COUNT);
    	totalIncome.setParameter(1, gymId);
    	totalIncome.setParameter(2, userSelectedDateForSMS,TemporalType.DATE);
		return totalIncome.getSingleResult();
    }
    
    private Object getTotalRenewalCount(String gymId,Date userSelectedDateForSMS){
    	Query totalIncome = em.createNativeQuery(AdminQueryHelper.RENEWAL_COUNT);
    	totalIncome.setParameter(1, gymId);
    	totalIncome.setParameter(2, userSelectedDateForSMS,TemporalType.DATE);
		return totalIncome.getSingleResult();
    }
    
    private Object getTotalAmntDuePaid(String gymId,Date userSelectedDateForSMS){
    	Query totalAmntDue = em.createNativeQuery(AdminQueryHelper.SUMMARY_MAIL_TOTAL_DUE);
    	totalAmntDue.setParameter(1, gymId);
    	totalAmntDue.setParameter(2, userSelectedDateForSMS,TemporalType.DATE);
    	totalAmntDue.setParameter(3, "AMOUNT_DUE_PAYMENT");
		return totalAmntDue.getSingleResult();
    }
    
    private Object[] getCashCardPayment(String gymId,Date userSelectedDateForSMS){
    	Query totalAmntDue = em.createNativeQuery(AdminQueryHelper.SUMMARY_MAIL_TOTAL_CASH_CARD);
    	totalAmntDue.setParameter(1, gymId);
    	totalAmntDue.setParameter(2, userSelectedDateForSMS,TemporalType.DATE);
    	totalAmntDue.setParameter(3, userSelectedDateForSMS,TemporalType.DATE);
    	totalAmntDue.setParameter(4, userSelectedDateForSMS,TemporalType.DATE);
		return (Object[]) totalAmntDue.getResultList().get(0);
    }
    
    private Object getTotalSalesTillDateForThisMonth(String gymId,Date userSelectedDateForSMS){
    	Query totalSales = em.createNativeQuery(AdminQueryHelper.TOTAL_SALES_TILL_DATE_FOR_THIS_MONTH);
    	totalSales.setParameter(1, gymId);
    	totalSales.setParameter(2, userSelectedDateForSMS,TemporalType.DATE);
    	totalSales.setParameter(3, userSelectedDateForSMS,TemporalType.DATE);
    	totalSales.setParameter(4, userSelectedDateForSMS,TemporalType.DATE);
    	totalSales.setParameter(5, userSelectedDateForSMS,TemporalType.DATE);
    	totalSales.setParameter(6, userSelectedDateForSMS,TemporalType.DATE);
    	totalSales.setParameter(7, userSelectedDateForSMS,TemporalType.DATE);
    	
    	return totalSales.getSingleResult();
    }
    
    private Object getTotalExpenseTillDateForThisMonth(String gymId,Date userSelectedDateForSMS){
    	Query totalExpense = em.createNativeQuery(AdminQueryHelper.TOTAL_EXPENSE_TILL_DATE_FOR_THIS_MONTH);
    	totalExpense.setParameter(1, gymId);
    	totalExpense.setParameter(2, userSelectedDateForSMS,TemporalType.DATE);
    	return totalExpense.getSingleResult();
    }

        
    private String createSMSTemplate(String gymName ,String totalIncome,String enrollCount,String renewalCount,String amountDuePaid,String totalSalesTillDate,String memberFootfall,String staffFootfall, String totalCashPayment, String totalCardPayment, String totalChqPayment, String totalExpenseTillDateForThisMonth, String totalProfitTillDateForThisMonth,Date userSelectedDate ){
    	StringBuilder summarySmsStringBuilder = new StringBuilder();
    	summarySmsStringBuilder.append("Summary For ").append(gymName).append(" On "+DateUtil.getStringFromDate(userSelectedDate,"dd/MM/yyyy\r\n"));
    	summarySmsStringBuilder.append("Total Income For Today :Rs. ").append(totalIncome).append("\r\nPaid By Cash:").append(totalCashPayment).append("\r\nPaid By Card:").append(totalCardPayment).append("\r\nPaid By Chq:").append(totalChqPayment).append("\r\nEnrollment : ").append(enrollCount).append("\r\nRenewal : ").append(renewalCount);
    	summarySmsStringBuilder.append("\r\nAmountDuePaid :Rs. ").append(amountDuePaid).append("\r\nMembers Footfall: ").append(memberFootfall).append("\r\nStaff Footfall:").append(staffFootfall).append("\r\nMonthly Sales Till Date :Rs. ").append(totalSalesTillDate).append("\r\nMonthly Expense Till Date :Rs. ").append(totalExpenseTillDateForThisMonth).append("\r\nMonthly Profit Till Date :Rs. ").append(totalProfitTillDateForThisMonth);
    	if(!gymName.startsWith("IMPACT"))
    		summarySmsStringBuilder.append("\r\nPlease login to www.myconnectpedia.com to view the details.-Team Connectpedia");
    	else
    		summarySmsStringBuilder.append("\r\nPlease login to Impact Fitness ERP to view the details.-Team Impact Fitness");
    	String smsSubj = summarySmsStringBuilder.toString();
    	return smsSubj;
    }
    
    private String createSMSTemplateForTomorrowsReminder(String gymName,Integer tomorrowAmountDueCount,Integer tomorrowsExpiryCount,Integer tomorrowsEnqFollowUpCount){
    	StringBuilder reminderSMSStringBuilder = new StringBuilder();
    	reminderSMSStringBuilder.append("Reminder For Tomorrow: ").append("\r\nTotal AmountDue Count :").append(tomorrowAmountDueCount);
    	reminderSMSStringBuilder.append("\r\nExpiry Count : ").append(tomorrowsExpiryCount).append("\r\nEnquiry FollowUp Count : "+tomorrowsEnqFollowUpCount);
    	if(!gymName.startsWith("IMPACT"))
    		reminderSMSStringBuilder.append("\r\nPlease login to www.myconnectpedia.com ERP to view the details.-Team Connectpedia");
    	else
    		reminderSMSStringBuilder.append("\r\nPlease login to Impact Fitness ERP to view the details.-Team Impact Fitness");
    	
    	return reminderSMSStringBuilder.toString();
    }

	@Override
	public void sendSummarySMSToOnwers(ConnectPediaTO requestResponseData) {
		
		FitnessGroomingCentre centreDetails = em.find(FitnessGroomingCentre.class,requestResponseData.getGym());
		String[] centreOwnerNums = centreDetails.getCentreOwnerNum().split(",");
		List<String> contactNums =  Arrays.asList(centreOwnerNums);		
		TriggerSMS triggerSMS = new TriggerSMS(contactNums, (String)requestResponseData.getUserRequest(), centreDetails.getWorkingKey(), centreDetails.getSenderIdForSMS());
		try{
			triggerSMS.triggerSMS((String)requestResponseData.getUserRequest());
		}catch(Exception exc){
			exc.printStackTrace();
		}
		
	}

	@Override
	public void createCentre(ConnectPediaTO requestResponseData) {
		FitnessCentreVO fitnessCentreVO = (FitnessCentreVO)requestResponseData.getUserRequest();
		AdminCentreCreationHelper adminCreationHelper = new AdminCentreCreationHelper();
		FitnessGroomingCentre newCentre = adminCreationHelper.executeMapper(fitnessCentreVO);
		em.persist(newCentre);
		Gateway gateway = adminCreationHelper.executeMapper(fitnessCentreVO, newCentre.getCentreId());
		em.persist(gateway);
		UserFeatureAccess userFeatureAccess = new UserFeatureAccess();
		userFeatureAccess.setAcessToAllFeatures("Y");
		userFeatureAccess.setIsException("N");
		userFeatureAccess.setUserId(gateway.getUserId());
		userFeatureAccess.setExceptionFeatureId(null);
		em.persist(userFeatureAccess);
	}

	@Override
	public ConnectPediaTO searchMembersForDietChartAddition(ConnectPediaTO requestRespData) {
		String userRequest = (String)requestRespData.getUserRequest();
		Boolean numberOrName = Character.isDigit(userRequest.charAt(0));
		Query queryForRetrieval = em.createNamedQuery("Enrollment.fetchMembersByContactNumOrName");
		if(numberOrName){
			queryForRetrieval.setParameter("contactNum", userRequest);	
			queryForRetrieval.setParameter("userName", null);
			queryForRetrieval.setParameter("memberID", userRequest);
		}else{
			queryForRetrieval.setParameter("contactNum", null);	
			queryForRetrieval.setParameter("userName", "%"+userRequest+"%");
			queryForRetrieval.setParameter("memberID", userRequest);
		}
		queryForRetrieval.setParameter("gymId", requestRespData.getGym());
		List<Enrollment> resultList = (List<Enrollment>)queryForRetrieval.getResultList();
		List<User> userList = new DietChartHelper().executeMapperForMemberSearch(resultList);
		requestRespData.setAppResponse(userList);
		return requestRespData;
	}
	
	@Override
	public ConnectPediaTO addMemberFeedback(ConnectPediaTO requestResponseData) {
		// TODO Auto-generated method stub
		MemberFeedbackVO memFeedback = (MemberFeedbackVO)requestResponseData.getUserRequest();
		//Enrollment memberForFeedback = em.find(Enrollment.class, user.getId());
		MemberFeedback memberWithFeedback = new MemFeedbackHelper().executeMapperForMemFeedbkAdd(memFeedback);
		memberWithFeedback.setCentreId(requestResponseData.getGym());
		try{
			em.persist(memberWithFeedback);
		}catch(Exception uniqueException){
			if(uniqueException instanceof MySQLIntegrityConstraintViolationException){
				requestResponseData.setExceptions(Boolean.TRUE);
			}
		}
		return requestResponseData;
//		if(memFeedback.getMemberEmail() != null && !memFeedback.getMemberEmail().isEmpty()){
//			FitnessGroomingCentre associatedGym = em.find(FitnessGroomingCentre.class,requestResponseData.getGym() );
//			AdminBeanRemote adminBeanRemote = ssnCntxt.getBusinessObject(AdminBeanRemote.class);
//			adminBeanRemote.sendFeedbackToOwnerAndMember(memFeedback.getMemberEmail(), memFeedback, associatedGym);
//		}
	}
	
//	@Override
//	public void sendFeedbackToOwnerAndMember(String emailTo, MemberFeedbackVO user, FitnessGroomingCentre associatedGym ) {
//		
//		String mailContent = "<html><body>Dear Mr. Owner, <br/> A Member has left a feedback for your gym "+associatedGym.getCentreName()+", "+associatedGym.getBranch()+". Please find the details bellow."
//				+ "<br/> Name: "+user.getMemberName()+""
//				+ "<br/> Phone: "+user.getMemberPhone()+""
//				+ "<br/> Feedback: "+user.getFeedbackDetails()+""
//				+ "<br/> FRONT_OFFICE: "+user.getFrontOffice()+""
//				+ "<br/> STAFF_FRIENDLINESS: "+user.getStaffFriendliness()+""
//				+ "<br/> CLEANLINESS: "+user.getCleanliness()+""
//				+ "<br/> FOOD_QUALITY: "+user.getFoodQuality()+""
//				+ "<br/> MUSIC_QUALITY: "+user.getMusicQuality()+""
//				+ "<br/> EQUIPMENTS: "+user.getEquipments()+""
//				+ "<br/> PERSONAL_TRAINING: "+user.getPersonalTraining()+""
//				+ "<br/> PARKING: "+user.getParking()+""
//				+ "<br/> LOCKER_WASHROOMS: "+user.getLockerAndWashroom()+""
//				+ "<br/><br/> ** CC to the Member";
//		String [] emailsList = formEmailToList(associatedGym.getEnrollmentMailRcvrId()+","+user.getMemberEmail());
//		MailHelper mailer = new MailHelper(emailsList,"","Member Feedback About Gym",mailContent,associatedGym.getEnrollmentMailSenderId(), associatedGym.getEnrollmentMailSenderPWD());
//		mailer.sendEmail();
//		
//	}
	
	private String [] formEmailToList(String emailToList){
		String [] emailsList = null;
		if(emailToList.contains(",")){
			emailsList = emailToList.split(",");
			return emailsList;
		}else{
			emailsList = new String[1];
			emailsList[0] = emailToList;
			return emailsList;
		}
	}

	@Override
	public ConnectPediaTO getFeedbackByDateRange(ConnectPediaTO requestRespData) {
		// TODO Auto-generated method stub
		Date[] dateRange = (Date[])requestRespData.getUserRequest();
		Query queryForEnrollbyDateRange = em.createNamedQuery("MemberFeedback.getAllFeedbacksByDateRange");
		queryForEnrollbyDateRange.setParameter("startDate",dateRange[0] , TemporalType.DATE);
		queryForEnrollbyDateRange.setParameter("endDate", dateRange[1], TemporalType.DATE);
		queryForEnrollbyDateRange.setParameter("centreId",requestRespData.getGym());
		List<MemberFeedback> resultList = (List<MemberFeedback>)queryForEnrollbyDateRange.getResultList();
		List<MemberFeedbackVO> feedbackList = new MemFeedbackHelper().executeMapperForMemFeedbkView(resultList);
		requestRespData.setAppResponse(feedbackList);
		return requestRespData;
	}

	@Override
	public ConnectPediaTO getAllFeedback(ConnectPediaTO requestRespData) {
		// TODO Auto-generated method stub
		Query queryForEnrollbyDateRange = em.createNamedQuery("MemberFeedback.getAllFeedbacks");
		queryForEnrollbyDateRange.setParameter("centreId", requestRespData.getGym());
		List<MemberFeedback> resultList = (List<MemberFeedback>)queryForEnrollbyDateRange.getResultList();
		List<MemberFeedbackVO> feedbackList = new MemFeedbackHelper().executeMapperForMemFeedbkView(resultList);
		requestRespData.setAppResponse(feedbackList);
		return requestRespData;
	}

	@Override
	public void addCentreBatchTiming(ConnectPediaTO requestResponseData) {
		// TODO Auto-generated method stub
		FitnessGroomingCentre fitCentreDtls = em.find(FitnessGroomingCentre.class, requestResponseData.getGym());
		List<CustomBatchesVO> btchList = (List<CustomBatchesVO>) requestResponseData.getUserRequest();
		BatchTimeHelper btchHelper = new BatchTimeHelper();
		FitnessGroomingCentre fitCentre = btchHelper.mapCustomBatchTiming(btchList, fitCentreDtls);
		em.persist(fitCentre);
	}

	@Override
	public ConnectPediaTO viewCentreBatchTiming(
			ConnectPediaTO requestResponseData) {
		// TODO Auto-generated method stub
		Query fetchCentreBatches = em.createNamedQuery("CustomBatches.viewBatches");
		fetchCentreBatches.setParameter("gymId", requestResponseData.getGym());
		List<CustomBatches> resultList = (List<CustomBatches>)fetchCentreBatches.getResultList();
		BatchTimeHelper btchHelper = new BatchTimeHelper();
		List<CustomBatchesVO> batchList = btchHelper.mapFetchBatchTiming(resultList);
		requestResponseData.setAppResponse(batchList);
		return requestResponseData;
	}

	@Override
	public ConnectPediaTO retrieveExistingMember(ConnectPediaTO requestRespData) {
		String userRequest = (String)requestRespData.getUserRequest();
		Query queryForRetrieval = em.createNamedQuery("Enrollment.fetchMembersByContactNum");
		queryForRetrieval.setParameter("contactNum", userRequest);	
		queryForRetrieval.setParameter("gymId", requestRespData.getGym());
		List<Enrollment> resultList = (List<Enrollment>)queryForRetrieval.getResultList();
		Enrollment enrollment = null;
		if(resultList != null && !resultList.isEmpty()){
			enrollment = resultList.get(0);
			String memberName = enrollment.getBasicDetails().getName();
			String lastName = enrollment.getBasicDetails().getLastName();
			String email = enrollment.getPersonalDetails().getEmailId();
			String fullDetails = memberName+"_"+lastName+"_"+email;
			requestRespData.setAppResponse(fullDetails);
		}
		
		return requestRespData;
	}

	@Override
	public ConnectPediaTO fetchReportBasedOnParameter(
			ConnectPediaTO requestRespData) {
		String centreId = (String)requestRespData.getGym();
		FeedbackReportVO feedbckReportVO = (FeedbackReportVO)requestRespData.getUserRequest();
		Query query = em.createNamedQuery("MemberFeedback.getParameterBasedReport");
		query.setParameter("ratingParameter", feedbckReportVO.getSelectedParameter());
		query.setParameter("centreId", centreId);
		query.setParameter("monthName", feedbckReportVO.getMonth());
		query.setParameter("year", feedbckReportVO.getYear().toString());
		List<MemberFeedback> resultList = (List<MemberFeedback>)query.getResultList();
		List<MemberFeedbackVO> memberFeedbackList = new MemFeedbackHelper().executeMapperForParameterBasedReport(resultList,feedbckReportVO.getSelectedParameter());
		requestRespData.setAppResponse(memberFeedbackList);
		return requestRespData;
	}

	@Override
	public ConnectPediaTO fetchConsolidatedReport(ConnectPediaTO requestRespData) {
		String centreId = (String)requestRespData.getGym();//ratingParameter
		FeedbackReportVO feedbckReportVO = (FeedbackReportVO)requestRespData.getUserRequest();
		Query query = em.createNamedQuery("MemberFeedback.getConsolidatedReport");
		query.setParameter("centreId", centreId);
		query.setParameter("monthName", feedbckReportVO.getMonth());
		query.setParameter("year", feedbckReportVO.getYear().toString());
		List<MemberFeedback> resultList = (List<MemberFeedback>)query.getResultList();
		List<MemberFeedbackVO> memberFeedbackList = new MemFeedbackHelper().executeMapperForConsolidatedReport(resultList);
		requestRespData.setAppResponse(memberFeedbackList);
		return requestRespData;
	}

	@Override
	public void configureGSTTax(ConnectPediaTO requestResponseData) {
		String centreId = (String)requestResponseData.getGym();
		TaxConfigVO taxConfigVO = (TaxConfigVO)requestResponseData.getUserRequest();
		FitnessGroomingCentre centre = em.find(FitnessGroomingCentre.class, centreId);
		centre.setSgst(taxConfigVO.getSgst());
		centre.setCgst(taxConfigVO.getCgst());
		centre.setGstNumber(taxConfigVO.getGstNumber());
		em.persist(centre);
	}

	@Override
	public void deleteBatch(ConnectPediaTO requestResponseData) {
//		String centreId = (String)requestResponseData.getGym();
		CustomBatchesVO customBatchesVO = (CustomBatchesVO)requestResponseData.getUserRequest();
		CustomBatches batchSelected = em.find(CustomBatches.class, customBatchesVO.getCentreBatchId());
		em.remove(batchSelected);
		
	}

	@Override
	public ConnectPediaTO updateClientRepresentative(ConnectPediaTO requestData) {
		// TODO Auto-generated method stub\
		Object[] reqData =  (Object[]) requestData.getUserRequest();
		List<Object[]> selectedFollowUpReports = (List<Object[]>) reqData[0];
		String newRepName = (String) reqData[1];
		Integer noOfClientRep = (Integer)reqData[2];
		
		/*for(Object[] eachRec : selectedFollowUpReports){
			if(!eachRec[8].toString().equalsIgnoreCase("Enquiry")){
				PackagePayment pkgPay = em.find(PackagePayment.class, Long.valueOf(eachRec[2].toString()));
				pkgPay.setClientRep(newRepName);
				em.persist(pkgPay);
			}else{
				EnquiryFollowUp enqFlw = em.find(EnquiryFollowUp.class, Long.valueOf(eachRec[2].toString()));
				enqFlw.setEnquiryBy(newRepName);
				em.persist(enqFlw);
			}
		}*/
		Integer count=0;
		for(Object[] eachRec : selectedFollowUpReports){
			if(count == noOfClientRep)
				break;
			else
				count++;
			
			if(!eachRec[8].toString().equalsIgnoreCase("Enquiry")){
				Enrollment enrollment = em.find(Enrollment.class, Long.valueOf(eachRec[1].toString()));
				enrollment.setClientRep(newRepName);
				for (PackagePayment ppi : enrollment.getPackagePymnt()) {
					ppi.setClientRep(newRepName);
					ppi.setBusinessBy(newRepName);
				}
				em.persist(enrollment);
			}else{
				Enquiry enq = em.find(Enquiry.class, Long.valueOf(eachRec[1].toString()));
				enq.setClientRep(newRepName);
				for (EnquiryFollowUp enqFollowUp : enq.getEnquiryFollowUp()) {
					enqFollowUp.setEnquiryBy(newRepName);
				}
				em.persist(enq);
				//em.persist(enqFollowUp);
			}
		}
		requestData.setAppResponse("success");
		return requestData;
	}

	@Override
	public ConnectPediaTO fetchUserAllAccess(ConnectPediaTO requestResponseData) {
		// TODO Auto-generated method stub
		String userId = (String) requestResponseData.getUserRequest();
		Gateway gateway = em.find(Gateway.class, userId);
		UserFeatureAccess userAccess = em.find(UserFeatureAccess.class, userId);
		AdminUserCreationHelper adminMapper = new AdminUserCreationHelper();
		NewUserVO newUser = adminMapper.executeMapperEntityToVo(gateway, userAccess);
		requestResponseData.setAppResponse(newUser);
		//System.out.println("line 720: "+ newUser.getUserName());
		return requestResponseData;
	}

	@Override
	public void updateUserAccess(ConnectPediaTO requestData) {
		// TODO Auto-generated method stub
		NewUserVO newUser = (NewUserVO) requestData.getUserRequest();
		//System.out.println(newUser.getUserType()+"|"+newUser.getFeaturesListEmployeeZone().length);
		
		//update Gateway Table
		Gateway gateway = em.find(Gateway.class, newUser.getUserName());
		AdminUserCreationHelper adminMapper = new AdminUserCreationHelper();
		Gateway gatewayUpdate = adminMapper.executeMapper(newUser,gateway, requestData.getGym());
		em.persist(gatewayUpdate);
		
		
		UserFeatureAccess userAccess = em.find(UserFeatureAccess.class, newUser.getUserName());
		em.remove(userAccess);
		
		UserFeatureAccess updateUserAccess = adminMapper.executeMapperForAccess(newUser);
		em.persist(updateUserAccess);
	}

	@Override
	public void createTicket(ConnectPediaTO requestResponseData) {
		TicketVO ticketVO =(TicketVO)requestResponseData.getUserRequest();
		String gymId = requestResponseData.getGym();
		TicketHelper ticketHelper = new TicketHelper();
		Ticket ticket = ticketHelper.executeMapper(ticketVO, gymId);
		em.persist(ticket);
		
	}
	
	@Override
	public void saveTODO(ConnectPediaTO requestResponseData) {
		List<TodoListVO> toDoList = new ArrayList();
		toDoList = (List<TodoListVO>) requestResponseData.getUserRequest();
		String gymId = requestResponseData.getGym();
		TodoListHelper todoListHelper = new TodoListHelper();
		List<TodoList> toDolist = todoListHelper.executeMapper(toDoList, gymId);
		for (TodoList todoList2 : toDolist) {
			em.persist(todoList2);
		}
		
	}
	
	@Override
	public void updateToDo(ConnectPediaTO requestResponseData) {
		List<TodoListVO> toDoList = new ArrayList();
		toDoList = (List<TodoListVO>) requestResponseData.getUserRequest();
		for (TodoListVO todoList2 : toDoList) {
		TodoList toDo = em.find(TodoList.class, todoList2.getId());
		toDo.setDone("Y");
		em.persist(toDo);
		}
		
	}

	@Override
	public ConnectPediaTO viewTicket(ConnectPediaTO requestData) {
		String centreId = requestData.getGym();
		Query query = null;
		if(centreId.equals("TEST_GYM")){
			query = em.createNamedQuery("Ticket.fetchAllTickets");
		}else{
			query = em.createNamedQuery("Ticket.fetchAllTicketsForCentre");
			query.setParameter("gymId", centreId);
		}
		List<Ticket> tickets = query.getResultList();
		TicketHelper ticketHelper = new TicketHelper();
		List<TicketVO> allTickets = ticketHelper.executeMapperForView(tickets);
		requestData.setAppResponse(allTickets);
		return requestData;
	}
	
	
	@Override
	public void assignAndUpdateTicket(ConnectPediaTO requestRespData) {
		TicketVO ticketVO =(TicketVO)requestRespData.getUserRequest();
		String gymId = requestRespData.getGym();
		Ticket ticket = em.find(Ticket.class,ticketVO.getTicketId());
		TicketHelper ticketHelper = new TicketHelper();
		ticket = ticketHelper.executeMapperForAssignUpdate(ticketVO, gymId,ticket);
		em.persist(ticket);
		
	}
	
	@Override
	public ConnectPediaTO viewTodoList(ConnectPediaTO requestData) {
		String centreId = requestData.getGym();
		Query query = null;
		query = em.createNamedQuery("TodoList.fetchAllToDoList");
		query.setParameter("gymId", centreId);
		
		List<TodoList> todoList = query.getResultList();
		TodoListHelper todoListHelper = new TodoListHelper();
		List<TodoListVO> allToDoList = todoListHelper.executeMapperForView(todoList);
		requestData.setAppResponse(allToDoList);
		return requestData;
	}
}

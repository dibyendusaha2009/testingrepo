package com.danovus.connectpedia.analytics.delegate;

import com.danovus.connectpedia.analytics.AnalyticsBeanRemote;
import com.danovus.cp.framework.data.ConnectPediaTO;
import com.danovus.cp.framework.delegate.CPDelegate;

public class AnalyticsBD extends CPDelegate {
	public ConnectPediaTO invokeBusinessProcess(ConnectPediaTO requestData,Boolean response){
		String serviceNameAction = requestData.getAction();
		AnalyticsBeanRemote analyticsBeanRemote = (AnalyticsBeanRemote) getService(serviceNameAction,requestData.getModule());
		if(serviceNameAction.equals("monthlyMissedRenewals"))
			return analyticsBeanRemote.fetchMonthlyMissedRenewals(requestData);
		else if(serviceNameAction.equals("quarterlyMissedRenewals"))
			return analyticsBeanRemote.fetchQuarterlyMissedRenewals(requestData);
		else if(serviceNameAction.equals("halfyearlyMissedRenewals")){
			return analyticsBeanRemote.fetchHalfyearlyMissedRenewals(requestData);
		}
		return null;
	}

}

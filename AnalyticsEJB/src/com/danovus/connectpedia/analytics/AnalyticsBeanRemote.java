package com.danovus.connectpedia.analytics;

import javax.ejb.Remote;

import com.danovus.cp.framework.data.ConnectPediaTO;

@Remote
public interface AnalyticsBeanRemote {

	ConnectPediaTO fetchMonthlyMissedRenewals(ConnectPediaTO requestResponseData);
	
	ConnectPediaTO fetchQuarterlyMissedRenewals(ConnectPediaTO requestResponseData);
	
	ConnectPediaTO fetchHalfyearlyMissedRenewals(ConnectPediaTO requestResponseData);
}

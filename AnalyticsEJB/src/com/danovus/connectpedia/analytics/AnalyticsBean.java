package com.danovus.connectpedia.analytics;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.danovus.connectpedia.analytics.helper.AnalyticsHelper;
import com.danovus.connectpedia.analytics.helper.AnalyticsQueryHelper;
import com.danovus.connectpedia.reporting.helper.ReportingBeanHelper;
import com.danovus.connectpedia.reporting.helper.ReportingQueryHelper;
import com.danovus.cp.framework.data.ConnectPediaTO;

/**
 * Session Bean implementation class AnalyticsBean
 */
@Stateless(name="analyticsBean", mappedName = "analyticsBean")
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class AnalyticsBean implements AnalyticsBeanRemote {
	
	@PersistenceContext(unitName="GYM_CONNECT")
	private EntityManager em;
	
	
    /**
     * Default constructor. 
     */
    public AnalyticsBean() {
    
    }

	@Override
	public ConnectPediaTO fetchMonthlyMissedRenewals(
			ConnectPediaTO requestResponseData) {
		Query createGenderRatioQuery = em.createNativeQuery(AnalyticsQueryHelper.GENDER_RATIO);
    	createGenderRatioQuery.setParameter(1, requestResponseData.getGym());
    	List<Object[]> gendersList = (List<Object[]>)createGenderRatioQuery.getResultList();
    	requestResponseData.setAppResponse(new AnalyticsHelper().executeMapperForMonthlyMissedRenewals();
    	return requestResponseData;
	}

	@Override
	public ConnectPediaTO fetchQuarterlyMissedRenewals(
			ConnectPediaTO requestResponseData) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConnectPediaTO fetchHalfyearlyMissedRenewals(
			ConnectPediaTO requestResponseData) {
		// TODO Auto-generated method stub
		return null;
	}

}
